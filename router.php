<?php
$urls = array(
    'home' => '/index.php?module=main&action=view',
    'articles' => '/index.php?module=article&action=list',
    'article' => '/index.php?module=article&action=view',
    'article_add' => '/index.php?module=article&action=add',
    'article_edit' => '/index.php?module=article&action=edit',
    'article_delete' => '/index.php?module=article&action=delete',
    'login' => '/index.php?module=auth&action=login',
    'logout' => '/index.php?module=auth&action=logout',
    'register' => '/index.php?module=auth&action=register',
    'about' => '/index.php?module=about&action=view',
    'image' => '/index.php?module=about&action=image',

    'admin' => '/index.php?module=admin&action=view',

    'admin_article' => '/index.php?module=admin_article&action=view',
    'admin_articles' => '/index.php?module=admin_article&action=list',
    'admin_article_add' => '/index.php?module=admin_article&action=add',
    'admin_article_edit' => '/index.php?module=admin_article&action=edit',
    'admin_article_delete' => '/index.php?module=admin_article&action=delete',

    'admin_statistic' => '/index.php?module=admin_statistic&action=view',
    'admin_statistic_edit' => '/index.php?module=admin_statistic&action=edit',
    'admin_statistic_delete' => '/index.php?module=admin_statistic&action=delete',

    'admin_user' => '/index.php?module=admin_user&action=view',
    'admin_users' => '/index.php?module=admin_user&action=list',
    'admin_user_add' => '/index.php?module=admin_user&action=add',
    'admin_user_edit' => '/index.php?module=admin_user&action=edit',
    'admin_user_delete' => '/index.php?module=admin_user&action=delete'
);

if (isset($_REQUEST['module'])) {
    switch ($_REQUEST['module']) {
        case 'main':
            $controller = $_SERVER['DOCUMENT_ROOT'].'/controllers/Main.php';
            break;
        case 'article':
            $controller = $_SERVER['DOCUMENT_ROOT'].'/controllers/ArticleController.php';
            break;
        case 'auth':
            $controller = $_SERVER['DOCUMENT_ROOT'].'/controllers/AuthController.php';
            break;
        case 'about':
            $controller = $_SERVER['DOCUMENT_ROOT'].'/controllers/AboutController.php';
            break;
        case 'admin':
            $controller = $_SERVER['DOCUMENT_ROOT'].'/controllers/admin/AdminHome.php';
            break;
        case 'admin_article':
            $controller = $_SERVER['DOCUMENT_ROOT'].'/controllers/admin/AdminArticleController.php';
            break;
        case 'admin_statistic':
            $controller = $_SERVER['DOCUMENT_ROOT'].'/controllers/admin/AdminStatisticController.php';
            break;
        case 'admin_user':
            $controller = $_SERVER['DOCUMENT_ROOT'].'/controllers/admin/AdminUserController.php';
            break;
    }
} else {
    $_REQUEST['module'] = 'main';
    $_REQUEST['action'] = 'view';
}
?>