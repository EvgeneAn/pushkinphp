<?php
header("Content-type: text/html;charset=utf-8");
global $urls;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <?php foreach ($styles as $style) { ?>
        <link rel="stylesheet" href="<?=$style?>" type="text/css">
    <?php } ?>

    <link rel="icon" type="iamge/x-icon" href="/statics/images/favicon.ico">
    <title>
        <?php echo $title; ?>        
    </title>
</head>

<body id="home">
<nav class="top-menu">
    <div class="container-fluid">
        <div class="col-1">
            <img src="/statics/images/pero.jpg" alt="Перо" class="logo">
        </div>
        <ul class="col-5">
            <li><a href="/index.php#home">Главная</a></li>
            <li><a href="/index.php#biography">Биография</a></li>
            <li ><a href="/index.php#news">Произведения</a></li>
            <?php
            if (isset($_SESSION['username'])) {
                echo '<li class="parent-menu">';
                echo '<a href="'.$urls['articles'].'">Статьи</a>';
                echo '<ul class="sub-menu sub-menu-hidden">';
                echo '<li><a href="'.$urls['article_add'].'">Добавить статью</a></li>';
                echo '<li><a href="'.$urls['articles'].'&username='
                .$_SESSION['username'].'">Мои статьи</a></li>';
                echo '</ul></li>';
            } else {
                echo '<li><a href="'.$urls['articles'].'">Статьи</a></li>';
            }
            ?>
            </li>
            <?php
                if (isset($_SESSION['username'])) {
                    if (isset($_SESSION['role']) && isset($_SESSION['role'])) {
                        if ($_SESSION['role'] == 1)
                            echo '<li><a href="'.$urls['admin'].'">Администрирование</a></li>';
                    }
                    echo '<li><a href="'.$urls['about'].'">О сайте</a></li>';
                    echo '<li><a href="'.$urls['logout'].'">Выход</a></li>';
                } else {
                    echo '<li><a href="'.$urls['login'].'">Вход</a></li>';
                    echo '<li><a href="'.$urls['register'].'">Регистрация</a></li>';
                }
            ?>
        </ul>
    </div>
</nav>
<div id="arrowTop" hidden="true"><i class="fas fa-arrow-up"></i></div>