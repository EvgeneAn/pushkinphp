<footer>
    <p class="center">А.С. Пушкин</p>
    <p class="center" id="clock"></p>
</footer>

<script>
  var clock_p = document.getElementById("clock");
  var date = new Date();
  clock_p.innerHTML = date.toLocaleTimeString();

  function clock() {
    var date = new Date();
    clock_p.innerHTML = date.toLocaleTimeString();
  }
  setInterval(clock, 1000);
</script>

<?php foreach ($scripts as $script) { ?>
  <script src="<?=$script?>"></script>
<?php }?>

<script src="https://kit.fontawesome.com/1c61574759.js" crossorigin="anonymous"></script>
<script>
if (window.innerHeight - 100 > document.body.clientHeight) {
    let footer = document.getElementsByTagName('footer');
    footer[0].classList.add('bottom-footer');
  }
</script>
</body>
</html>