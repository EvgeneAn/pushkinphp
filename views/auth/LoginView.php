<style>
    * {
        margin: 0px;
        padding: 0px;
    }

    #canvas {
        position: absolute;
        display: block;
        background-color: black;
        background-image: linear-gradient(160deg, #413500, #f87123)
    }
</style>
<canvas id=canvas></canvas>
<main>
    
<section class="container">
    <form class="form-register" action="" method="post">
        <legend>Вход</legend>
        <?php
            if (isset($_GET['register']) && $_GET['register'] == 'ok') {
                echo '<p style="color: #07ff07;">Регистрация прошла успешно.</p>';
            }
        ?>
        <div class="form-control">
            <?php
                if (isset($errors['db_error'])) {
                    echo '<p>'.$errors['db_error'].'</p>';
                }
            ?>
            <label for="username">Логин</label>
            <?php
                if (isset($_POST['username']))
                    echo '<input type="text" name="username" value="'.$_POST['username'].'">';
                else
                    echo '<input type="text" name="username">';

                if (isset($errors['username'])) {
                    if (count($errors['username']) > 0) {
                        echo '<div class="form-error"><span>';
                        foreach ($errors['username'] as $message) echo $message;
                        echo '</span></div>';
                    }
                }
            ?>
        </div>
        <div class="form-control">
            <label for="password">Пароль</label>
            <?php
                if (isset($_POST['password']))
                    echo '<input type="password" name="password" value="'.$_POST['password'].'">';
                else
                    echo '<input type="password" name="password">';

                if (isset($errors['password'])) {
                    if (count($errors['password']) > 0) {
                        echo '<div class="form-error"><span>';
                        foreach ($errors['password'] as $message) echo $message;
                        echo '</span></div>';
                    }
                }
            ?>
        </div>

        <input type="submit" value="Войти" name="btn_ok">
    </form>
</section>
</main>
<script src="/statics/js/FloatGrid.js"></script>