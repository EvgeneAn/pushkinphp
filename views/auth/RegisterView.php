<main>   
<section class="container">
<form class="form-register" action="" method="post">
    <legend>Регистрация</legend>
    <?php
        if (isset($errors['db_error'])) {
            echo '<p>'.$errors['db_error'].'</p>';
        }
    ?>
    <div class="form-control">
        <label for="username">Логин</label>
        <?php
            if (isset($_POST['username']))
                echo '<input type="text" name="username" value="'.$_POST['username'].'">';
            else
                echo '<input type="text" name="username">';

            if (isset($errors['username'])) {
                if (count($errors['username']) > 0) {
                    echo '<div class="form-error"><span>';
                    foreach ($errors['username'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="email">Почта</label>
        <?php
            if (isset($_POST['email']))
                echo '<input type="text" name="email" value="'.$_POST['email'].'">';
            else
                echo '<input type="text" name="email">';

            if (isset($errors['email'])) {
                if (count($errors['email']) > 0) {
                    echo '<div class="form-error"><span>';
                    foreach ($errors['email'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="password">Пароль</label>
        <?php
            if (isset($_POST['password']))
                echo '<input type="password" name="password" value="'.$_POST['password'].'">';
            else
                echo '<input type="password" name="password">';

            if (isset($errors['password'])) {
                if (count($errors['password']) > 0) {
                    echo '<div class="form-error"><span>';
                    foreach ($errors['password'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="password2">Повторите пароль</label>
        <?php
            if (isset($_POST['password2']))
                echo '<input type="password" name="password2" value="'.$_POST['password2'].'">';
            else
                echo '<input type="password" name="password2">';

            if (isset($errors['password2'])) {
                if (count($errors['password2']) > 0) {
                    echo '<div class="form-error"><span>';
                    foreach ($errors['password2'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>

    <input type="submit" value="Зарегистрироваться" name="btn_ok">
</form>
</section>
</main>