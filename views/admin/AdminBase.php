<?php
$styles = array(
    '/statics/css/style.css',
    '/statics/css/grid.css',
    '/statics/css/text.css'
);

$scripts = array(
    '/statics/js/scroll.js',
);

require $_SERVER['DOCUMENT_ROOT'].'/views/admin/header.php';
if (isset($_REQUEST['notification'])) {
    echo '<section id="message"><p>'.$_REQUEST['notification'].'</p></section>';
}

?>

<div class="container-fluid admin-text" style="margin-top: 60px;">
    <div class="col-2">
        <ul class="admin-menu">
        <?php
            global $urls;
            if (strcmp($_REQUEST['module'], 'admin') != 0) {
                echo '<li><a href="'.$urls['admin'].'">Администрирование</a></li>';
            }
            echo '<li><a href="'.$urls['admin_articles'].'">Статьи</a></li>';
            echo '<li><a href="'.$urls['admin_users'].'">Пользователи</a></li>';
            echo '<li><a href="'.$urls['admin_statistic'].'">Статистика</a></li>';
        ?>
        </ul>
    </div>
    
    
<?php
require $view;
?>
</div>

<?php
require $_SERVER['DOCUMENT_ROOT'].'/views/footer.php';
?>