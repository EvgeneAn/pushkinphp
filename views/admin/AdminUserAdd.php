<div class="col-4">
<form class="form-admin" action="" method="post">
    <legend><?=$title?></legend>
    <?php
        if (isset($errors['db_error'])) {
            echo '<p>'.$errors['db_error'].'</p>';
        }
    ?>
    <div class="form-control">
        <label for="username">Логин</label>
        <?php
            if (isset($_POST['username']))
                echo '<input type="text" name="username" value="'.$_POST['username'].'">';
            else if (isset($object->username))
                echo '<input type="text" name="username" value="'.$object->username.'">';
            else echo '<input type="text" name="username">';

            if (isset($errors['username'])) {
                if (count($errors['username']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['username'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="email">Почта</label>
        <?php
            if (isset($_POST['email']))
                echo '<input type="text" name="email" value="'.$_POST['email'].'">';
            else if (isset($object->email))
                echo '<input type="text" name="email" value="'.$object->email.'">';
            else echo '<input type="text" name="email">';

            if (isset($errors['email'])) {
                if (count($errors['email']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['email'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="role">Роль</label>
        <select name="role">
        <?php
            if (isset($_POST['role'])) {
                if ($_POST['role'] == 0) {
                    echo '<option value="0" selected>Пользователь</option>';
                    echo '<option value="1">Администратор</option>';
                } else if ($_POST['role'] == 1) {
                    echo '<option value="0">Пользователь</option>';
                    echo '<option value="1" selected>Администратор</option>';
                }
            } else if (isset($object->role)) {
                if ($object->role == 0) {
                    echo '<option value="0" selected>Пользователь</option>';
                    echo '<option value="1">Администратор</option>';
                } else if ($object->role == 1) {
                    echo '<option value="0">Пользователь</option>';
                    echo '<option value="1" selected>Администратор</option>';
                }
            } else {
                echo '<option value="0" selected>Пользователь</option>';
                echo '<option value="1">Администратор</option>';
            }
        ?>
        </select>
        <?php
            if (isset($errors['role'])) {
                if (count($errors['role']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['role'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="date_registration">Дата регистрации</label>
        <?php
            if (isset($_POST['date_registration'])) {
                echo '<input type="date" value="'.$_POST['date_registration'].'" name="date_registration">';
            } else if (isset($object->date_registration)) {
                echo '<input type="date" value="'.$object->date_registration.'" name="date_registration">';
            } else {
                echo '<input type="date" name="date_registration">';
            }
            
            if (isset($errors['date_registration'])) {
                if (count($errors['date_registration']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['date_registration'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="password">Пароль</label>
        <?php
            if (isset($_POST['password']))
                echo '<input type="password" name="password" value="'.$_POST['password'].'">';
            else
                echo '<input type="password" name="password">';

            if (isset($errors['password'])) {
                if (count($errors['password']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['password'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="password2">Повторите пароль</label>
        <?php
            if (isset($_POST['password2']))
                echo '<input type="password" name="password2" value="'.$_POST['password2'].'">';
            else
                echo '<input type="password" name="password2">';

            if (isset($errors['password2'])) {
                if (count($errors['password2']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['password2'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <input type="submit" value="<?=$btn_text?>" name="btn_ok">
</form>
</div>