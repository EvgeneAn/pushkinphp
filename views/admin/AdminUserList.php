<div class="col-4">
<?php
echo '<h2><a href="'.$urls['admin_user_add'].'">Добавить пользователя</a></h2>';
if ($errors['get_rows']) {
    $all = $object->getAll();
    
    echo '<table class="statistics table-users">';
    echo '<thead>';
    echo '<tr>';
    echo '<th>ID</th>';
    echo '<th>Логин</th>';
    echo '<th>Почта</th>';
    echo '<th>Дата регистрации</th>';
    echo '<th></th>';
    echo '<th></th>';
    echo '</tr>';

    echo '<tbody>';
    foreach ($all as $user) {
        echo '<tr>';
        echo '<td>'.$user['id'].'</td>';
        echo '<td><a href="'.$urls['admin_user'].'&id='.$user['id'].'">'
             .$user['username'].'</a></td>';

        echo '<td>'.$user['email'].'</td>';
        echo '<td>'.$user['date_registration'].'</td>';
        echo '<td><a href="'.$urls['admin_user_edit'].'&id='.$user['id']
             .'">Редактировать</a></td>';
             
        echo '<td><a href="'.$urls['admin_user_delete'].'&id='.$user['id']
             .'">Удалить</a></td>';
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';

} else echo '<h3>Пользователей нет.</h3>';
?>
</div>