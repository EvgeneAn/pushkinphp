<div class="col-4">
<form class="form-admin" action="" method="post">
    <legend><?=$title?></legend>
    <?php
        if (isset($errors['db_error'])) {
            echo '<p>'.$errors['db_error'].'</p>';
        }
    ?>
    <div class="form-control">
        <label for="client_ip">IP клиента</label>
        <?php
            if (isset($_POST['client_ip']))
                echo '<input type="text" name="client_ip" value="'.$_POST['client_ip'].'">';
            else {
                if (isset($statistic)) echo '<input type="text" name="client_ip" value="'
                                          .$statistic->client_ip.'">';
                else echo '<input type="text" name="client_ip">';
            }

            if (isset($errors['client_ip'])) {
                if (count($errors['client_ip']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['client_ip'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="browser">Браузер клиента</label>
        <?php
            if (isset($_POST['browser']))
                echo '<input type="text" name="browser" value="'.$_POST['browser'].'">';
            else {
                if (isset($statistic)) echo '<input type="text" name="browser" value="'
                                          .$statistic->browser.'">';
                else echo '<input type="text" name="browser">';
            }

            if (isset($errors['browser'])) {
                if (count($errors['browser']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['browser'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="urn">Посещаемая страница</label>
        <?php
            if (isset($_POST['urn']))
                echo '<input type="text" name="urn" value="'.$_POST['urn'].'">';
            else {
                if (isset($statistic)) echo '<input type="text" name="urn" value="'
                                          .$statistic->urn.'">';
                else echo '<input type="text" name="urn">';
            }

            if (isset($errors['urn'])) {
                if (count($errors['urn']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['urn'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="date">Дата посещения</label>
        <?php
            if (isset($_POST['date'])) {
                echo '<input type="date" value="'.$_POST['date'].'" name="date">';
            } else if (isset($statistic->date)) {
                $date = explode(' ', $statistic->date);
                echo '<input type="date" value="'.$date[0].'" name="date">';
            } else {
                echo '<input type="date" name="date">';
            }

            if (isset($errors['date'])) {
                if (count($errors['date']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['date'] as $message) echo $message;
                    echo '</span></div>';
                }
            }

            echo '<label for="time">Время посещения</label>';
            if (isset($_POST['time'])) {
                echo '<input type="time" value="'.$_POST['time'].'" name="time">';
            } else if (isset($statistic->time)) {
                $time = explode(':', $statistic->time);
                $time = $time[0].':'.$time[1];
                echo '<input type="time" value="'.$time.'" name="time">';
            } else {
                echo '<input type="time" name="time">';
            }

            if (isset($errors['time'])) {
                if (count($errors['time']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['time'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="count">Кол-во посещений</label>
        <?php
            if (isset($_POST['count']))
                echo '<input type="number" min="0" name="count" value="'.$_POST['count'].'">';
            else {
                if (isset($statistic)) echo '<input type="number" min="0" name="count" value="'
                                            .$statistic->count.'">';
                else echo '<input type="number" min="0" name="count">';
            }

            if (isset($errors['count'])) {
                if (count($errors['count']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['count'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <br>
    <input type="submit" value="<?=$btn_text?>" name="btn_ok">
</form>
</div>