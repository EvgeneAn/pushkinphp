<?php
    echo '<div class="col-4">';
    echo '<h2><a href="'.$urls['admin_article_add'].'">Добавить статью</a></h2>';
    echo '<h3 class="center">'.$article->id.' - '.$article->title.'</h3>';

    $username = isset($article->username) ? $article->username : 'без автора';
    
    echo '<p>Автор: '.$username.'</p>';
    echo '<p>Дата публикации: '.$article->pub_date.'</p>';
    echo '<p>Дата последнего редактирования: '.$article->edit_date.'</p>';

    if (isset($_SESSION['id_user'])) {
        echo '<a href="'.$urls['admin_article_edit'].'&id='.$article->id.'">Редактировать</a>';
        echo '<a href="'.$urls['admin_article_delete'].'&id='.$article->id.'">Удалить</a>';
        echo '<br/>';
    }
    echo '<hr>';
    $text = htmlspecialchars($article->text);
    echo '<code>'.$text.'</code>';
    echo '</div>';
?>