<?php
header("Content-type: text/html;charset=utf-8");
global $urls;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <?php foreach ($styles as $style) { ?>
        <link rel="stylesheet" href="<?=$style?>" type="text/css">
    <?php } ?>

    <link rel="icon" type="iamge/x-icon" href="/statics/images/favicon.ico">
    <title>
        <?php echo $title; ?>        
    </title>
</head>

<body id="home">
<nav class="top-menu">
    <div class="container-fluid">
        <div class="col-1">
            <img src="/statics/images/pero.jpg" alt="Перо" class="logo">
        </div>
        <ul class="col-5">
            <li><a href="/index.php">На сайт</a></li>
            <?php
                if (isset($_SESSION['username'])) {
                    echo '<li><a href="'.$urls['logout'].'">Выход</a></li>';
                }
            ?>
        </ul>
    </div>
</nav>
<div id="arrowTop" hidden="true"><i class="fas fa-arrow-up"></i></div>