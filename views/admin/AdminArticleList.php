<div class="col-4">
<?php
echo '<h2><a href="'.$urls['admin_article_add'].'">Добавить статью</a></h2>';
if ($errors['get_rows']) {
    $all = $articles->getAll();
    foreach ($all as $article) {
        echo '<div class="col-6"><hr>';
        echo '<h3>'.$article['id'].'-'.$article['title'].'</h3>';
        echo '<p>Дата публикации: '.$article['pub_date'].'</p>';
        echo '<p>Дата последнего редактирования: '.$article['edit_date'].'</p>';
        echo '<a href="'.$urls['admin_article'].'&id='.$article['id'].'">Перейти</a>';

        if (isset($_SESSION['id_user'])) {
            echo '<a href="'.$urls['admin_article_edit'].'&id='.$article['id'].'">Редактировать</a>';
            echo '<a href="'.$urls['admin_article_delete'].'&id='.$article['id'].'">Удалить</a>';
        }
        echo '</div>';
    }
} else echo '<h3>Статей нет.</h3>';
?>
</div>