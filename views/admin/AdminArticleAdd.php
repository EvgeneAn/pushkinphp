<div class="col-4">
<form class="form-admin" action="" method="post">
    <legend><?=$title?></legend>
    <?php
        if (isset($errors['db_error'])) {
            echo '<p>'.$errors['db_error'].'</p>';
        }
    ?>
    <div class="form-control">
        <label for="title">Заголовок статьи</label>
        <?php
            if (isset($_POST['title']))
                echo '<input type="text" name="title" value="'.$_POST['title'].'">';
            else {
                if (isset($article)) echo '<input type="text" name="title" value="'
                                          .$article->title.'">';
                else echo '<input type="text" name="title">';
            }

            if (isset($errors['title'])) {
                if (count($errors['title']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['title'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="text">Текст</label>
        <?php
            if (isset($_POST['text']))
                echo '<textarea name="text" rows="11">'.$_POST['text'].'</textarea>';
            else {
                if (isset($article)) echo '<textarea name="text" rows="11">'
                                          .$article->text.'</textarea>';
                else echo '<textarea name="text" rows="11"></textarea>';
            }

            if (isset($errors['text'])) {
                if (count($errors['text']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['text'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="id_user">Полльзователь</label>
        <select name="id_user">
            <option selected value="0">Без пользователя</option>
        <?php
            if (isset($_POST['id_user'])) {
                foreach($users->getAll() as $user) {
                    if ($_POST['id_user'] == $user['id']) {
                        echo '<option selected value="'.$user['id'].'">'.$user['id']
                             .': '.$user['username'].'</option>';
                    } else {
                        echo '<option value="'.$user['id'].'">'.$user['id'].': '
                             .$user['username'].'</option>';
                    }
                }
            }
            else {
                foreach($users->getAll() as $user) {
                    if (isset($article->id_user) && $article->id_user == $user['id']) {
                        echo '<option selected value="'.$user['id'].'">'
                             .$user['id'].': '.$user['username'].'</option>';
                    } else {
                        echo '<option value="'.$user['id'].'">'.$user['id'].': '
                             .$user['username'].'</option>';
                    }
                }
            }
        ?>
        </select>
    </div>
    <div class="form-control">
        <label for="pub_date">Дата публикации</label>
        <?php
            if (isset($_POST['pub_date'])) {
                echo '<input type="date" value="'.$_POST['pub_date'].'" name="pub_date">';
            } else if (isset($article->pub_date)) {
                $date = explode(' ', $article->pub_date);
                echo '<input type="date" value="'.$date[0].'" name="pub_date">';
            } else {
                echo '<input type="date" name="pub_date">';
            }

            if (isset($errors['pub_date'])) {
                if (count($errors['pub_date']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['pub_date'] as $message) echo $message;
                    echo '</span></div>';
                }
            }

            echo '<label for="pub_time">Время публикации</label>';
            if (isset($_POST['pub_time'])) {
                echo '<input type="time" value="'.$_POST['pub_time'].'" name="pub_time">';
            } else if (isset($article->pub_date)) {
                $time = explode(' ', $article->pub_date)[1];
                $time = explode(':', $time);
                $time = $time[0].':'.$time[1];
                echo '<input type="time" value="'.$time.'" name="pub_time">';
            } else {
                echo '<input type="time" name="pub_time">';
            }

            if (isset($errors['pub_time'])) {
                if (count($errors['pub_time']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['pub_pub_timedate'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="edit_date">Дата редактирования</label>
        <?php
            if (isset($_POST['edit_date'])) {
                echo '<input type="date" value="'.$_POST['edit_date'].'" name="edit_date">';
            } else if (isset($article->edit_date)) {
                $date = explode(' ', $article->edit_date);
                echo '<input type="date" value="'.$date[0].'" name="edit_date">';
            } else {
                echo '<input type="date" name="edit_date">';
            }
            
            if (isset($errors['edit_date'])) {
                if (count($errors['edit_date']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['edit_date'] as $message) echo $message;
                    echo '</span></div>';
                }
            }

            echo '<label for="edit_time">Время редактирования</label>';
            if (isset($_POST['edit_time'])) {
                echo '<input type="time" value="'.$_POST['edit_time'].'" name="edit_time">';
            } else if (isset($article->edit_date)) {
                $time = explode(' ', $article->edit_date)[1];
                $time = explode(':', $time);
                $time = $time[0].':'.$time[1];
                echo '<input type="time" value="'.$time.'" name="edit_time">';
            } else {
                echo '<input type="time" name="edit_time">';
            }

            if (isset($errors['edit_time'])) {
                if (count($errors['edit_time']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['edit_time'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <input type="submit" value="<?=$btn_text?>" name="btn_ok">
</form>
</div>