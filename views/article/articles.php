<?php
    global $urls;

    if ($errors['get_rows']) {
        $all = $articles->getAll();
        foreach ($all as $article) {
            $text = mb_substr($article['text'], 0, 400, 'UTF-8');
            $text = strip_tags($text, '<p></p>');
            $text .= '...';
            echo '<div class="col-6"><hr>';
            echo '<h2>'.$article['title'].'</h2>';
            echo '<p>'.$text.'</p>';
            echo '<p>Дата публикации: '.$article['pub_date'].'</p>';
            echo '<p>Дата последнего редактирования: '.$article['edit_date'].'</p>';
            echo '<a href="'.$urls['article'].'&id='.$article['id'].'">Перейти</a>';
    
            if (isset($_SESSION['id_user']) && $article['id_user'] == $_SESSION['id_user']) {
                echo '<a href="'.$urls['article_edit'].'&id='.$article['id'].'">Редактировать</a>';
                echo '<a href="'.$urls['article_delete'].'&id='.$article['id'].'">Удалить</a>';
            }
            echo '</div>';
        }
    } else echo '<h3>У вас пока что нет статей.</h3>';
?>