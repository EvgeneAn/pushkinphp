<main>
<form class="form" action="" method="post">
    <legend><?=$title?></legend>
    <?php
        if (isset($errors['db_error'])) {
            echo '<p>'.$errors['db_error'].'</p>';
        }
    ?>
    <div class="form-control">
        <label for="title">Заголовок статьи</label>
        <?php
            if (isset($_POST['title']))
                echo '<input type="text" name="title" value="'.$_POST['title'].'">';
            else {
                if (isset($article)) echo '<input type="text" name="title" value="'
                                          .$article->title.'">';
                else echo '<input type="text" name="title">';
            }

            if (isset($errors['title'])) {
                if (count($errors['title']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['title'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <div class="form-control">
        <label for="text">Текст</label>
        <?php
            if (isset($_POST['text']))
                echo '<textarea name="text" rows="11">'.$_POST['text'].'</textarea>';
            else {
                if (isset($article)) echo '<textarea name="text" rows="11">'
                                          .$article->text.'</textarea>';
                else echo '<textarea name="text" rows="11"></textarea>';
            }

            if (isset($errors['text'])) {
                if (count($errors['text']) > 0) {
                    echo '<div><span>';
                    foreach ($errors['text'] as $message) echo $message;
                    echo '</span></div>';
                }
            }
        ?>
    </div>
    <input type="submit" value="<?=$btn_text?>" name="btn_ok">
</form>
</main>