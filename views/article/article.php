<?php
    $username = isset($article->username) ? $article->username : 'без автора';
    echo '<div class="col-6">';
    echo '<h2 class="center">'.$article->title.'</h2>';
    echo '<p>Автор: '.$username.'</p>';
    echo '<p>Дата публикации: '.$article->pub_date.'</p>';
    echo '<p>Дата последнего редактирования: '.$article->edit_date.'</p>';

    if (isset($_SESSION['id_user']) && $article->id_user == $_SESSION['id_user']) {
        echo '<a href="'.$urls['article_edit'].'&id='.$article->id.'">Редактировать</a>';
        echo '<a href="'.$urls['article_delete'].'&id='.$article->id.'">Удалить</a>';
        echo '<br/>';
    }
    echo '<hr>';
    echo $article->text;
    echo '</div>';
?>