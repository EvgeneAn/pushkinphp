<main class="about">
    <form action="" method="post" class="form">
        <div>
            <label for="filename">Имя файла</label>
            <?php
                if (isset($_GET['filename']))
                    echo '<input type="text" name="filename" value="'.$_GET['filename'].'">';
                else echo '<input type="text" name="filename">';
            ?>
        </div>
        <input type="submit" value="Определить размер" name="btn_ok">
    </form>

    <div class="center">
        <?php
            if (isset($_REQUEST['filesize']) && isset($_REQUEST['path'])) {
                echo '<p>Путь: '.$_REQUEST['path'].' <br/>Размер: '
                     .$_REQUEST['filesize'].' байт.</p>';
            } else echo '<p>Нет такого файла/каталога.</p>';
        ?>
    </div>
    
    <div id="statistics">
        <hr>
        <h2 class="center">Статистика посещений</h2>
        <?php
            global $urls, $statistics;
            $statistics->showTable();
            foreach ($statistics->getDiagrams() as $diagram) {
                echo '<img src="'.$urls['image'].'&image='.$diagram.'" class="img-center"/>';
            }
        ?>
    </div>
</main>