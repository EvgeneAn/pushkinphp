<?php
$styles = array(
    '/statics/css/style.css',
    '/statics/css/grid.css',
    '/statics/css/text.css'
);

$scripts = array(
    '/statics/js/scroll.js',
    '/statics/js/menu.js'
);

require $_SERVER['DOCUMENT_ROOT'].'/views/header.php';

if (isset($_REQUEST['notification'])) {
    echo '<section id="message"><p>'.$_REQUEST['notification'].'</p></section>';
}

require $view;

require $_SERVER['DOCUMENT_ROOT'].'/views/footer.php';
?>