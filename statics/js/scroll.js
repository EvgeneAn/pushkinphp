// Получить все ссылки на якоря. Якоря могут быть вида
// href="#..." и href="path#...", скрипт отберет только
// те якоря, которые предназначены для текущей страницы

// определить имя файла текущей страницы
let url = window.location.href;
url = url.substring(url.lastIndexOf('/') + 1) + '#';

// если в url содержиться якорная ссылка, убрать ее
if (url.includes('#')) {
    url = url.split('#')[0];
}


// выбрать все ссылки для текущей страницы
let anchors = document.querySelectorAll('a[href*="' + url + '#"]');
let anchors2 = document.querySelectorAll('a[href^="#"]');
anchors = Array.prototype.slice.call(anchors);
anchors2 = Array.prototype.slice.call(anchors2);

// соединить массивы
for (let anchor of anchors2) {
    if (!anchors.find(item => item == anchor)) {
        anchors.push(anchor);
    }
}


// выбрать только те якоря, которые расположены на текущей странице
for (let anchor of anchors) {
    // определить новый обработчик события click
    anchor.addEventListener('click', function(e) {
        // установить действие по умолчанию (отменит переход к якорю)
        e.preventDefault()

        // найти блок, к которому будет выполняться прокручивание
        const blockID = anchor.getAttribute('href').split('#')[1];

        // прокрутить текущий контейнер к блоку
        document.getElementById(blockID).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        })
    })
}

// прокрутка наверх
arrowTop.onclick = function() {
    document.querySelector('body').scrollIntoView({
        behavior: 'smooth'
    })
}

// скрытие/появление кнопки для прокрутки наверх
window.addEventListener('scroll', function() {
    arrowTop.hidden = (pageYOffset < document.
        documentElement.clientHeight);
})