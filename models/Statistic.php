<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/Model.php';

class Statistic extends Model {
    public $id;
    public $client_ip;
    public $browser;
    public $urn;
    public $date;
    public $time;
    public $count;
    public $total_sum_ip;
    public $sum_field;

    function __construct() {
        parent::__construct();
        $this->table_name = 'statistic';

        $this->errors = array();
        $this->errors['client_ip'] = array();
        $this->errors['browser'] = array();
        $this->errors['urn'] = array();
        $this->errors['date'] = array();
        $this->errors['time'] = array();
        $this->errors['count'] = array();
        $this->errors['db_error'] = '';
        $this->errors['duplicate'] = false;
        $this->errors['get_rows'] = false;
    }

    function isValid($data, $validate) {
        $valid = true;
        if (isset($data['client_ip'])) {
            if (!preg_match('|^(\\d{1,3}\\.){3}\\d{1,3}$|', $data['client_ip'],
                            $matches)) {
                $message = 'Указан Некорректный IPv4.';
                array_push($this->errors['client_ip'], $message);
                $valid = false;
            }
        } else {
            $message = 'IP не указан.';
            array_push($this->errors['client_ip'], $message);
            $valid = false;
        }

        if (isset($data['browser'])) {
            if (strlen($data['browser']) == 0) {
                $message = 'Браузер не указан.';
                array_push($this->errors['browser'], $message);
                $valid = false;
            }
        } else {
            $message = 'Браузер не указан.';
            array_push($this->errors['browser'], $message);
            $valid = false;
        }

        if (isset($data['urn'])) {
            if (strlen($data['urn']) == 0) {
                $message = 'URN не указан';
                array_push($this->errors['urn'], $message);
                $valid = false;
            }
        } else {
            $message = 'URN не указан.';
            array_push($this->errors['urn'], $message);
            $valid = false;
        }

        if (isset($data['date'])) {
            if (!preg_match('/^\\d{4}-(0\\d||1[0-2])-([0-2]\\d||3[0-1])$/',
                            $data['date'], $matches)) {
                $message = 'Указана неверная дата.';
                array_push($this->errors['date'], $message);
                $valid = false;
            }
        } else {
            $message = 'Дата не указана.';
            array_push($this->errors['date'], $message);
            $valid = false;
        }

        if (isset($data['time'])) {
            if (!preg_match('/^([0-1]\\d||2[0-3]):[0-5]\\d$/',
                            $data['time'], $matches)) {
                $message = 'Время указано некорректно.';
                array_push($this->errors['time'], $message);
                $valid = false;
            }
        } else {
            $message = 'Время не указано.';
            array_push($this->errors['time'], $message);
            $valid = false;
        }

        if (in_array('count', $validate)) {
            if (isset($data['count'])) {
                if (is_numeric($data['count'])) {
                    if ((int)$data['count'] < 0) {
                        $message = 'Кол-во посещений не должно быть отрицательным.';
                        array_push($this->errors['count'], $message);
                        $valid = false;
                    }
                } else {
                    $message = 'Кол-во посещений должно быть числом.';
                    array_push($this->errors['count'], $message);
                    $valid = false;
                }
            } else {
                $message = 'Кол-во посещений не указано.';
                array_push($this->errors['count'], $message);
                $valid = false;
            }
        }

        $date_str = $data['date'].' '.$data['time'];
        $date = new DateTime($date_str);
        $now = new DateTime('now');

        if ($date > $now) {
            $message = 'Дата и время посещения не может быть позднее текущей даты и времени';
            array_push($this->errors['date'], $message);
            array_push($this->errors['time'], $message);
            $valid = false;
        }

        if ($valid) $this->bind($data);
        return $valid;
    }

    function insert($columns) {
        if ($this->getIsReady()) {
            // найти запись с такими же значениями client_ip, browser, urn
            $filter = array('client_ip' => $this->client_ip,
                            'browser' => $this->browser,
                            'urn' => $this->urn,
                            'date' => $this->date);
            $count = $this->count;
            $res = $this->get($filter);
            $errors = $this->getErrors();

            // запись была найдена
            if ($res && $this->errors['get_rows']) {
                $this->count += $count;
                $this->time = date('H:i');

                $filter = ['client_ip' => $this->client_ip,
                           'browser' => $this->browser,
                           'urn' => $this->urn,
                           'date' => $this->date];
                $res = $this->update(['time', 'count'], $filter);
                if ($res) return true;
            }

            // запись не была найдена
            $query = $this->getInsertQuery($columns);
            $bind_str = $this->getBindStr($columns);

            if ($stmt = $this->connection->prepare($query[0])) {
                if ($stmt->bind_param($bind_str, ...array_values($query[1]))) {
                    if ($stmt->execute()) {
                        $this->id = $stmt->insert_id;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    function get(array $filter) {
        if ($this->getIsReady()) {
            $query = 'SELECT id, client_ip, browser, urn, date, time, '
                     .'count FROM '.$this->table_name.' WHERE ';
            
            foreach ($filter as $field => $value) {
                $type = gettype($value);
                if (strcmp($type, 'string') == 0) $query .= $field.'=\''.$value.'\' AND ';
                else $query .= $field.'='.$value.' AND ';
            }
            $query = substr_replace($query, '', -5);
            
            if ($stmt = $this->connection->prepare($query)) {
                if ($stmt->execute()) {
                    $stmt->store_result();
                    if ($stmt->num_rows == 0) {
                        $message = 'Запись не найдена.';
                        $this->errors['db_error'] = $message;
                    } else {
                        $stmt->bind_result($this->id, $this->client_ip,
                                           $this->browser, $this->urn,
                                           $this->date, $this->time,
                                           $this->count);
                        $stmt->fetch();
                        $this->errors['get_rows'] = true;
                    }
                    return true;
                }
            }
        } else {
            $message = 'Не удалось установить соединение с БД';
            $this->errors['db_error'] = $message;
        }

        return false;
    }

    function filter(array $filter) {}

    function orderBy($fields, $filter, $desc) {
        $this->all = array_splice($this->all, 0);

        if ($this->getIsReady()) {
            $query = 'SELECT ';
            foreach ($fields as $field) {
                $query .= $field.', ';
            }
            $query = substr_replace($query, '', -2);

            $query .= ' FROM '.$this->table_name.' ORDER BY '.$filter;
            
            if ($desc) $query .= ' DESC';
    
            if ($result = $this->connection->query($query)) {
                if ($result->num_rows > 0) {
                    $this->all = $result->fetch_all(MYSQLI_ASSOC);
                    $this->errors['get_rows'] = true;
                    return true;
                }
            }
        } else {
            $message = 'Не удалось установить соединение с БД';
            $this->errors['db_error'] = $message;
        }

        return false;
    }

    function total_sum_ip() {
        $this->all = array_splice($this->all, 0);
        $this->errors['get_rows'] = false;

        if ($this->getIsReady()) {
            $query = 'SELECT client_ip, SUM(count) AS total_sum_ip FROM '.$this->table_name
            .' GROUP BY client_ip';
    
            if ($result = $this->connection->query($query)) {
                if ($result->num_rows > 0) {
                    $this->total_sum_ip = $result->fetch_all(MYSQLI_ASSOC);
                    $this->errors['get_rows'] = true;
                    return true;
                }
            }
        } else {
            $message = 'Не удалось установить соединение с БД';
            $this->errors['db_error'] = $message;
        }

        return false;
    }

    function sumField($field) {
        unset($this->sum_field);
        $this->errors['get_rows'] = false;

        if ($this->getIsReady()) {
            $query = 'SELECT '.$field.', SUM(count) AS total_sum FROM '.$this->table_name
                     .' GROUP BY '.$field;
    
            if ($result = $this->connection->query($query)) {
                if ($result->num_rows > 0) {
                    $this->sum_field = $result->fetch_all(MYSQLI_ASSOC);
                    $this->errors['get_rows'] = true;
                    return true;
                }
            }
        } else {
            $message = 'Не удалось установить соединение с БД';
            $this->errors['db_error'] = $message;
        }

        return false;
    }

    protected function bind($data) {
        if (isset($data['id'])) $this->id = $data['id'];
        if (isset($data['client_ip'])) $this->client_ip = $data['client_ip'];
        if (isset($data['browser'])) $this->browser = $data['browser'];
        if (isset($data['urn'])) $this->urn = $data['urn'];
        if (isset($data['date'])) $this->date = $data['date'];
        if (isset($data['time'])) $this->time = $data['time'];
        if (isset($data['count'])) $this->count = $data['count'];
    }

    protected function getBindStr($columns) {
        $bind_str = '';
        foreach($columns as $column) {
            switch ($column) {
                case 'id':
                    $bind_str .= 'i';
                    break;
                case 'client_ip':
                    $bind_str .= 's';
                    break;
                case 'browser':
                    $bind_str .= 's';
                    break;
                case 'urn':
                    $bind_str .= 's';
                    break;
                case 'date':
                    $bind_str .= 's';
                    break;
                case 'time':
                    $bind_str .= 's';
                    break;
                case 'count':
                    $bind_str .= 'i';
                    break;
            }
        }
        return $bind_str;
    }
}
?>