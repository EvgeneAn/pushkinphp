<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/Model.php';

class Article extends Model {
    public $id;
    public $title;
    public $pub_date;
    public $edit_date;
    public $text;
    public $id_user;
    public $username;
    protected $table_join;

    function __construct() {
        parent::__construct();
        $this->table_name = 'article';
        $this->table_join = 'user';

        $this->errors = array();
        $this->errors['title'] = array();
        $this->errors['pud_date'] = array();
        $this->errors['edit_date'] = array();
        $this->errors['text'] = array();
        $this->errors['pub_date'] = array();
        $this->errors['pub_time'] = array();
        $this->errors['edit_date'] = array();
        $this->errors['edit_time'] = array();
        $this->errors['id_user'] = array();
        $this->errors['db_error'] = '';
        $this->errors['duplicate'] = false;
        $this->errors['get_rows'] = false;
    }

    function isValid($data, $validate) {
        $valid = true;
        $allowed_tags = ['p', 'a', 'img', 'div', 'hr', 'br', 'strong', 'h1',
                         'h2', 'h3', 'h4', 'h5', 'h6', 'i', 'ruby', 'rt',
                         'rp', 'mark', 'u', 'sup', 'sub', 'strike', 's',
                         'q', 'b', 'ul', 'li'];

        if (in_array('title', $validate)) {
            if (isset($data['title'])) {
                if (strlen($data['title']) < 3) {
                    $message = 'Длина заголовка должна быть больше 3 символов.';
                    array_push($this->errors['title'], $message);
                    $valid = false;
                }
            } else {
                $message = 'Поле "Заголовок статьи" обязательно."';
                array_push($this->errors['title'], $message);
                $valid = false;
            }
        }

        if (in_array('text', $validate)) {
            if (isset($data['text'])) {
                $text = strip_tags($data['text'], $allowed_tags);
                $data['text'] = $text;

                if (strlen($data['text']) < 50) {
                    $message = 'Длина статьи должна быть больше 49 символов.';
                    array_push($this->errors['text'], $message);
                    $valid = false;
                }
            } else {
                $message = 'Поле "Текст" должно быть заполнено.';
                array_push($this->errors['text'], $message);
                $valid = false;
            }
        }

        if (in_array('id_user', $validate)) {
            if (isset($data['id_user'])) {
                if (!is_numeric($data['id_user'])) {
                    $message = 'Поле "Пользователь" заполнено неккоректно.';
                    array_push($this->errors['id_user'], $message);
                    $valid = false;
                }
            }
        }

        if (isset($data['pub_date'])) {
            if (!preg_match('/^\\d{4}-(0\\d||1[0-2])-([0-2]\\d||3[0-1])$/',
                            $data['pub_date'], $matches)) {
                $message = 'Дата указана некорректно.';
                array_push($this->errors['pub_date'], $message);
                $valid = false;
            }
        }
        
        if (isset($data['pub_time'])) {
            if (!preg_match('/^([0-1]\\d||2[0-3]):[0-5]\\d$/',
                            $data['pub_time'], $matches)) {
                $message = 'Время указано некорректно.';
                array_push($this->errors['pub_time'], $message);
                $valid = false;
            }
        }

        if (isset($data['edit_date'])) {
            if (!preg_match('/^\\d{4}-(0\\d||1[0-2])-([0-2]\\d||3[0-1])$/',
                            $data['edit_date'], $matches)) {
                $message = 'Дата указана некорректно.';
                array_push($this->errors['edit_date'], $message);
                $valid = false;
            }
        }
        
        if (isset($data['edit_time'])) {
            if (!preg_match('/^([0-1]\\d||2[0-3]):[0-5]\\d$/',
                            $data['edit_time'], $matches)) {
                $message = 'Время указано некорректно.';
                array_push($this->errors['edit_time'], $message);
                $valid = false;
            }
        }

        if (isset($data['pub_date']) && isset($data['pub_time']) 
                && isset($data['eidt_date']) && isset($data['edit_time'])) {
            $str_pub_date = $data['pub_date'].' '.$data['pub_time'];
            $pub_date = new DateTime($str_pub_date);
            $str_edit_date = $data['edit_date'].' '.$data['edit_time'];
            $edit_date = new DateTime($str_edit_date);
    
            if ($pub_date > $edit_date) {
                $message = 'Дата и время редактирования должны быть позднее '
                            .'даты и времени публикации.';
                array_push($this->errors['edit_date'], $message);
                array_push($this->errors['edit_time'], $message);
                $valid = false;
            }
        }

        if ($valid) $this->bind($data);

        return $valid;
    }

    function insert($columns) {
        if ($this->getIsReady()) {
            $query = $this->getInsertQuery($columns);

            $bind_str = $this->getBindStr($columns);

            if ($stmt = $this->connection->prepare($query[0])) {
                if ($stmt->bind_param($bind_str, ...array_values($query[1]))) {
                    if ($stmt->execute()) {
                        $this->id = $stmt->insert_id;
                        $this->errors['duplicate'] = false;
                        return true;
                    } else {
                        if (strpos($stmt->error, 'Duplicate') !== false) {
                            $this->localeError($stmt->error);
                            $this->errors['duplicate'] = true;
                            return true;
                        }
                    }
                }
            }
        } else {
            $message = 'Не удалось установить соединение с БД';
            $this->errors['db_error'] = $message;
        }
        return false;
    }

    function get(array $filter) {
        if ($this->getIsReady()) {
            $query = 'SELECT a.id, a.title, a.pub_date, a.edit_date, a.text, '
                    .'a.id_user, u.username FROM '.$this->table_name.' AS a LEFT JOIN '
                    .$this->table_join.' AS u ON a.id_user=u.id WHERE ';
            
            foreach ($filter as $field => $value) {
                $type = gettype($value);
                if (strcmp($type, 'string') == 0) $query .= $field.'=\''.$value.'\' AND ';
                else $query .= $field.'='.$value.' AND ';
            }
            $query = substr_replace($query, '', -5);
            
            if ($stmt = $this->connection->prepare($query)) {
                if ($stmt->execute()) {
                    $stmt->store_result();
                    if ($stmt->affected_rows == 0) {
                        $message = 'Статья с id='.$value.' не найдена.';
                        $this->errors['db_error'] = $message;
                    } else {
                        $stmt->bind_result($this->id, $this->title,
                                           $this->pub_date, $this->edit_date,
                                           $this->text, $this->id_user,
                                           $this->username);
                        $stmt->fetch();
                        $this->errors['get_rows'] = true;
                    }
                    return true;
                }
            }
        } else {
            $message = 'Не удалось установить соединение с БД';
            $this->errors['db_error'] = $message;
        }

        return false;
    }

    function filter(array $filter) {
        $this->all = array_splice($this->all, 0);

        if ($this->getIsReady()) {
            $query = 'SELECT '.$this->table_name.'.id, title, pub_date, edit_date, text, '
            .'id_user, username FROM '.$this->table_name.' JOIN '
            .$this->table_join.' ON id_user = '.$this->table_join.'.id WHERE ';
            
            // подставить поля и их значения для сортировки.
            foreach ($filter as $field => $value) {
                $type = gettype($value);
                if (strcmp($type, 'integer') == 0) $query .= $field.'='.$value.' AND ';
                else $query .= $field.'=\''.$value.'\' AND ';
            }
            $query = substr_replace($query, '', -5);
            
            if ($result = $this->connection->query($query)) {
                if ($result->num_rows > 0) {
                    // Заполнение массива
                    $this->all = $result->fetch_all(MYSQLI_ASSOC);
                    $this->errors['get_rows'] = true;
                    return true;
                }
                else {
                    $this->errors['get_rows'] = false;
                    return true;
                }
            } else $this->errors['db_error'] = 'Возникла ошибка';

        } else {
            $message = 'Не удалось установить соединение с БД';
            $this->errors['db_error'] = $message;
        }
        return false;
    }

    protected function bind($data) {
        if (isset($data['id'])) $this->id = $data['id'];
        if (isset($data['title'])) $this->title = $data['title'];

        if (isset($data['pub_date']) && isset($data['pub_date']))
            $this->pub_date = $data['pub_date'].' '.$data['pub_time'];
        
        if (isset($data['edit_date']) && isset($data['edit_date']))
            $this->edit_date = $data['edit_date'].' '.$data['edit_time'];
        
        if (isset($data['text'])) $this->text = $data['text'];
        
        if (isset($data['id_user'])) {
            if ($data['id_user'] == 0)
                $this->id_user = null;
            else $this->id_user = $data['id_user'];
        }
    }

    protected function getBindStr($columns) {
        $bind_str = '';
        foreach($columns as $column) {
            switch ($column) {
                case 'id':
                    $bind_str .= 'i';
                    break;
                case 'title':
                    $bind_str .= 's';
                    break;
                case 'pub_date':
                    $bind_str .= 's';
                    break;
                case 'edit_date':
                    $bind_str .= 's';
                    break;
                case 'text':
                    $bind_str .= 's';
                    break;
                case 'id_user':
                    $bind_str .= 'i';
                    break;
            }
        }
        return $bind_str;
    }

    /**
     * Переводит ошибку БД с английского на русский язык.
     */
    private function localeError($message) {
        if (preg_match('|Duplicate entry \'([^\']+)\'.*title.*|i', $message, $matches,
                       PREG_OFFSET_CAPTURE)) {
                $error_message = 'Статья с заголовком "'.$matches[1][0].'" уже существует.';
                $this->errors['db_error'] = $error_message;
            }
    }
}
?>