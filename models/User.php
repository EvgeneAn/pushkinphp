<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/Model.php';

class User extends Model {
    public $id;
    public $username;
    public $first_name;
    public $last_name;
    public $email;
    public $password;
    public $date_registration;
    public $role; // 0 - user / 1 - admin

    function __construct() {
        parent::__construct();

        $this->table_name = 'user';

        $this->errors = array();
        $this->errors['username'] = array();
        $this->errors['first_name'] = array();
        $this->errors['last_name'] = array();
        $this->errors['email'] = array();
        $this->errors['password'] = array();
        $this->errors['password2'] = array();
        $this->errors['date_registration'] = array();
        $this->errors['role'] = array();
        $this->errors['db_error'] = '';
        $this->errors['duplicate'] = false;
        $this->errors['get_rows'] = false;
    }

    /**
     * Проверяет данные $data. Если поля нет в массиве $validate,
     * то поле не будет проверяться.
     */
    function isValid($data, $validate) {
        $valid = true;
        if (in_array('username', $validate)) {
            if (isset($data['username'])) {
                if (strlen($data['username']) <= 3) {
                    $message = 'Длина логина должна быть больше 3 символов. ';
                    array_push($this->errors['username'], $message);
                    $valid = false;
                }
                
                if (!preg_match('|^[a-z0-9_]+$|iu', $data['username'], $matches)) {
                    $message = 'Логин должен состоять только из символов латинского алфавитов, арабских цифр и символа _';
                    array_push($this->errors['username'], $message);
                    $valid = false;
                }
            } else {
                $message = 'Поле "Логин" не заполнено.';
                array_push($this->errors['username'], $message);
                $valid = false;
            }
        }
        
        if (in_array('first_name', $validate)) {
            if (isset($data['first_name'])) {
                if (strlen($data['first_name']) <= 2) {
                    $message = 'Длина имени должна быть больше 2 символов. ';
                    array_push($this->errors['first_name'], $message);
                    $valid = false;
                }
                
                if (!preg_match('|^[a-zа-я]+$|iu', $data['first_name'], $matches)) {
                    $message = 'Имя должно состоять только из символов латинского и русского алфавитов';
                    array_push($this->errors['first_name'], $message);
                    $valid = false;
                }
            }
        }
        
        if (in_array('last_name', $validate)) {
            if (isset($data['last_name'])) {
                if (strlen($data['last_name']) <= 2) {
                    $message = 'Длина фамилии должна быть больше 2 символов. ';
                    array_push($this->errors['last_name'], $message);
                    $valid = false;
                }
                
                if (!preg_match('|^[a-zа-я]+$|iu', $data['last_name'], $matches)) {
                    $message = 'Фамилия должна состоять только из символов латинского и русского алфавитов';
                    array_push($this->errors['last_name'], $message);
                    $valid = false;
                }
            }
        }

        if (in_array('email', $validate)) {
            if (isset($data['email'])) {
                if (!preg_match('|^[a-z0-9][a-z0-9\._-]*[a-z0-9]@[a-z]+\.[a-z]{2,}$|i',
                                $data['email'], $matches)) {
                    $message = 'Введена не корректная почта.';
                    array_push($this->errors['email'], $message);
                    $valid = false;
                }
            } else {
                $message = 'Поле "Почта" не заполнено.';
                array_push($this->errors['email'], $message);
                $valid = false;
            }
        }

        if (in_array('password', $validate)) {
            if (isset($data['password'])) {
                if (strlen($data['password']) < 8) {
                    $message = 'Пароль должен состоять минимум из 8 символов. ';
                    array_push($this->errors['password'], $message);
                    $valid = false;
                }
                
                if (!preg_match('|^[a-z0-9=+.,_?#%@!*-]{8,}$|i', $data['password'],
                                $matches)) {
                    $message = 'Пароль должен состоять только из символов латинского алфавита и может содержать символы: =+.,_?#%@!*-';
                    array_push($this->errors['password'], $message);
                    $valid = false;
                }
            } else {
                $message = 'Поле "Пароль" не заполнено.';
                array_push($this->errors['password'], $message);
                $valid = false;
            }
        }

        if (in_array('password2', $validate)) {
            if (isset($data['password2'])) {
                if (strcmp($data['password'], $data['password2']) != 0) {
                    $message = 'Пароли не совпадают.';
                    array_push($this->errors['password2'], $message);
                    $valid = false;
                }
            } else {
                $message = 'Необходимо повторно ввести пароль.';
                array_push($this->errors['password2'], $message);
                $valid = false;
            }
        }
        
        if (in_array('role', $validate)) {
            if (isset($data['role'])) {
                if ($data['role'] < 0 && $data['role'] > 1) {
                    $message = 'Указана некорректная роль.';
                    array_push($this->errors['role'], $message);
                    $valid = false;
                }
            } else {
                $message = 'Роль пользователя не указана.';
                array_push($this->errors['role'], $message);
                $valid = false;
            }
        }

        if (isset($data['date_registration'])) {
            if (!preg_match('/^\\d{4}-(0\\d||1[0-2])-([0-2]\\d||3[0-1])$/',
                            $data['date_registration'], $matches)) {
                $message = 'Дата указана некорректно.';
                array_push($this->errors['date_registration'], $message);
                $valid = false;
            }
        }

        if (isset($data['date_registration'])) {
            $date = new DateTime($data['date_registration']);
            $now = new DateTime('now');
    
            if ($date > $now) {
                $message = 'Дата регистрации должна быть не позднее текущей даты.';
                array_push($this->errors['date_registration'], $message);
                $valid = false;
            }
        }
        
        if ($valid) $this->bind($data);
        
        return $valid;
    }

    protected function getBindStr($columns) {
        $bind_str = '';
        foreach($columns as $column) {
            switch ($column) {
                case 'id':
                    $bind_str .= 'i';
                    break;
                case 'username':
                    $bind_str .= 's';
                    break;
                case 'first_name':
                    $bind_str .= 's';
                    break;
                case 'last_name':
                    $bind_str .= 's';
                    break;
                case 'email':
                    $bind_str .= 's';
                    break;
                case 'password':
                    $bind_str .= 's';
                    break;
                case 'date_registration':
                    $bind_str .= 's';
                    break;
                case 'role':
                    $bind_str .= 'i';
                    break;
            }
        }
        return $bind_str;
    }

    protected function bind($data) {
        if (isset($data['id'])) $this->id = $data['id'];
        if (isset($data['username'])) $this->username = $data['username'];
        if (isset($data['first_name'])) $this->first_name = $data['first_name'];
        if (isset($data['last_name'])) $this->last_name = $data['last_name'];
        if (isset($data['email'])) $this->email = $data['email'];
        if (isset($data['password'])) $this->setPassword($data['password']);
        if (isset($data['role'])) $this->role = $data['role'];

        if (isset($data['date']) && isset($data['time'])) 
            $this->date_registration = $data['date'].' '.$data['time'];

        if (isset($data['date_registration'])) 
            $this->date_registration = $data['date_registration'];
    }

    function insert($columns) {
        if ($this->getIsReady()) {
            $this->errors['duplicate'] = true;
            $query = $this->getInsertQuery($columns);
            $bind_str = $this->getBindStr($columns);

            if ($stmt = $this->connection->prepare($query[0])) {
                if ($stmt->bind_param($bind_str, ...array_values($query[1]))) {
                    if ($stmt->execute()) {
                        $this->id = $stmt->insert_id;
                        $this->errors['duplicate'] = false;
                        return true;
                    } else {
                        if (strpos($stmt->error, 'Duplicate') !== false) {
                            $this->localeError($stmt->error);
                            return true;
                        }
                    }
                }
            }
        } else {
            $message = 'Не удалось установить соединение с БД';
            $this->errors['db_error'] = $message;
        }
        return false;
    }

    function get(array $filter) {
        if ($this->getIsReady()) {
            $query = 'SELECT id, username, email, date_registration, password, role FROM '
                     .$this->table_name.' WHERE ';
            
            foreach ($filter as $field => $value) {
                $type = gettype($value);
                if (strcmp($type, 'string') == 0) $query .= $field.'=\''.$value.'\' AND ';
                else $query .= $field.'='.$value.' AND ';
            }
            $query = substr_replace($query, '', -5);
            
            if ($stmt = $this->connection->prepare($query)) {
                if ($stmt->execute()) {
                    $stmt->store_result();
                    if ($stmt->num_rows == 0) {
                        $message = 'Пользователь не найден.';
                        $this->errors['db_error'] = $message;
                    } else {
                        $stmt->bind_result($this->id, $this->username,
                                           $this->email, $this->date_registration,
                                           $this->password, $this->role);
                        $stmt->fetch();
                        $this->errors['get_rows'] = true;
                    }
                    return true;
                }
            }
        } else {
            $message = 'Не удалось установить соединение с БД';
            $this->errors['db_error'] = $message;
        }

        return false;
    }

    function filter(array $filter) {}

    function setPassword($password) {
        $this->password = hash('sha256', $password);
    }

    function checkPassword($password) {
        $hash_pwd = hash('sha256', $password);
        if (strcmp($this->password, $hash_pwd) == 0) return true;
        else {
            $message = 'Пароль или логин был введен не правильно.';
            array_push($this->errors['password'], $message);
            return false;
        }
    }

    /**
     * Переводит ошибку БД с английского на русский язык.
     */
    private function localeError($message) {
        if (preg_match('|Duplicate entry \'([^\']+)\'.*username.*|i', $message, $matches,
                       PREG_OFFSET_CAPTURE)) {
                $error_message = 'Пользователь с логином '.$matches[1][0].' уже существует.';
                $this->errors['db_error'] = $error_message;
            }
        
        if (preg_match('|Duplicate entry \'([^\']+)\'.*email.*|i', $message, $matches,
                       PREG_OFFSET_CAPTURE)) {
                $error_message = 'Пользователь с почтой '.$matches[1][0].' уже существует.';
                $this->errors['db_error'] = $error_message;
            }
    }
}
?>