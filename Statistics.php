<?php
require_once 'jpgraph/jpgraph.php';
require_once 'jpgraph/jpgraph_pie.php';
require_once 'jpgraph/jpgraph_pie3d.php';

require_once $_SERVER['DOCUMENT_ROOT'].'/models/Statistic.php';

/**
 * Класс для учета статистики посещаемости сайта.
 * Записывает данные о клиентах в файл.
 */
class Statistics {
    private $projectName;
    public $clientIP;
    public $browserNameVersion;
    public $date;
    public $time;
    public $URN;
    public $all;
    public $diagrams;
    public $total_sum_ip = array();
    public $sum_ip;

    public function getDiagrams() {
        return $this->diagrams;
    }

    function __construct() {
        $this->diagrams = array(
            'ip',
            'browsers',
            'months',
            'times'
        );

        $this->projectName = 'pushkinphp';
        $date_now = new DateTime();
        $this->date = $date_now->format('Y-m-d');
        $this->time = $date_now->format('H:i');
        $this->URN = $this->getURN();

        $this->getClientIP();
        $this->getBrowser();

        $this->insertStat();
    }

    /**
     * Метод для определения IP-адреса клиента.
     */
    private function getClientIP() {
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            // Глобальный IP клиента
            $this->clientIP = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            // IP / цепочка IP, устанавливаемая прокси серверами
            $this->clientIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            // IP клиента / IP прокси сервера
            $this->clientIP = $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Метод для определения названия и версии браузера.
     */
    private function getBrowser() {
        $agent = $_SERVER['HTTP_USER_AGENT'];
        preg_match("/(MSIE|Opera|Firefox|Chrome|Version)(?:\/| )([0-9.]+)/", $agent, $matcher);
        $this->browserNameVersion = ($matcher[1] == "Version") ? "Safari" : $matcher[1];
        $this->browserNameVersion .= '/'.$matcher[2];
    }

    /**
     * Возвращает URN для статистики
     */
    private function getURN() {
        $modules = ['main', 'article', 'auth', 'about'];
        $actions = ['view', 'list', 'edit', 'add', 'register',
                    'delete', 'login', 'logout', 'image'];

        $URN = 'index.php?module=main&action=view';

        if (isset($_REQUEST['module']) && isset($_REQUEST['action'])) {
            if (in_array($_REQUEST['module'], $modules) && in_array($_REQUEST['action'], $actions)) {
                $URN = 'index.php?module='.$_REQUEST['module'].'&action='.$_REQUEST['action'];
            }
        }
        return $URN;
    }

    /**
     * Метод для записи строки о текущем посещении в БД.
     */
    private function insertStat() {
        $statistic = new Statistic();
        $data = [
            'client_ip' => $this->clientIP,
            'browser' => $this->browserNameVersion,
            'urn' => $this->URN,
            'date' => $this->date,
            'time' => $this->time,
            'count' => 1
        ];
        $validate = ['client_ip', 'browser', 'urn', 'date', 'time', 'count'];

        $valid = $statistic->isValid($data, $validate);
        if ($valid) {
            $statistic->insert($validate);
        }
    }
    
    public function allStat() {
        unset($this->all);
        unset($this->total_sum_ip);

        $statistic = new Statistic();
        $fields = ['id', 'client_ip', 'browser', 'urn', 'date', 'time', 'count'];
        $res = $statistic->all($fields);
        if ($res) $this->all = $statistic->getAll();

        $res = $statistic->total_sum_ip();
        if ($res) {
            foreach($statistic->total_sum_ip as $row) {
                $this->total_sum_ip[$row['client_ip']] = $row['total_sum_ip'];
            }
        }

        return $this->all;
    }

    public function orderBy($filter, $desc) {
        unset($this->all);
        unset($this->total_sum_ip);

        $statistic = new Statistic();
        $fields = ['id', 'client_ip', 'browser', 'urn', 'date', 'time', 'count'];
        $res = $statistic->orderBy($fields, $filter, $desc);
        if ($res) $this->all = $statistic->getAll();

        $res = $statistic->total_sum_ip();
        if ($res) {
            foreach($statistic->total_sum_ip as $row) {
                $this->total_sum_ip[$row['client_ip']] = $row['total_sum_ip'];
            }
        }

        return $this->all;
    }

    /**
     * Метод, создающий круговую диаграмму на основе IP адресов.
     */
    public function ip() {
        $statistic = new Statistic();
        $res = $statistic->sumField('client_ip');
        if ($res) {
            if (isset($statistic->sum_field)) {
                $sectors = array();
                $legends = array();
        
                // заполнить legends и sectors
                foreach ($statistic->sum_field as $row) {
                    $legends[] = $row['client_ip'];
                    $sectors[] = $row['total_sum'];
                }
    
                $this->createDiagram($sectors, $legends,
                    'Статистика посещений по IP адресам');
            }
        }
    }

    /**
     * Метод, создающий круговую диаграмму на основе браузеров.
     */
    public function browsers() {
        $statistic = new Statistic();
        $res = $statistic->sumField('browser');
        if ($res) {
            if (isset($statistic->sum_field)) {
                $sectors = array();
                $legends = array();
        
                // заполнить legends и sectors
                foreach ($statistic->sum_field as $row) {
                    $legends[] = $row['browser'];
                    $sectors[] = $row['total_sum'];
                }
    
                $this->createDiagram($sectors, $legends,
                    'Статистика посещений по браузерам');
            }
        }
    }

    /**
     * Метод, создающий круговую диаграмму на основе даты.
     */
    public function months() {
        $MONTHS = array(
            '01'=>'Январь',
            '02'=>'Февраль',
            '03'=>'Март',
            '04'=>'Апрель',
            '05'=>'Май',
            '06'=>'Июнь',
            '07'=>'Июль',
            '08'=>'Август',
            '09'=>'Сентябрь',
            '10'=>'Октябрь',
            '11'=>'Ноябрь',
            '12'=>'Декабрь',
        );
        $statistic = new Statistic();
        $res = $statistic->sumField('date');
        if ($res) {
            if (isset($statistic->sum_field)) {
                $sectors = array();
                $legends = array();
        
                // заполнить legends и sectors
                foreach ($statistic->sum_field as $row) {
                    $date = explode('-', $row['date'])[1];
                    $legends[] = $MONTHS[$date];
                    $sectors[] = $row['total_sum'];
                }
                $this->createDiagram($sectors, $legends,
                    'Статистика посещений по месецам');
            }
        }
    }

    /**
     * Метод, создающий круговую диаграмму на основе времени.
     */
    public function times() {
        $statistic = new Statistic();
        $res = $statistic->sumField('time');
        if ($res) {
            if (isset($statistic->sum_field)) {
                $sectors = array();
                $legends = array();
                $data = array();

                foreach($statistic->sum_field as $row) {
                    $time = explode(':', $row['time'])[0];
                    if (array_key_exists($time, $data)) {
                        $data[$time] += $row['total_sum'];
                    }
                    else {
                        $data[$time] = $row['total_sum'];
                    }
                }
        
                // заполнить legends и sectors
                foreach ($data as $time => $value) {
                    $legends[] = $time;
                    $sectors[] = $value;
                }
                $this->createDiagram($sectors, $legends,
                    'Статистика посещений по времени');
            }
        }
    }

    /**
     * Формирует диаграмму.
     * 
     * @param array $sectors сектора (в сумме должно быть 100)
     * @param array $legends подписи к секторам
     * @param string $title заголовок диаграммы
     */
    public function createDiagram($sectors, $legends, $title) {
        $graph = new PieGraph(500, 300);
        $graph->SetShadow();

        $graph->title->Set($title);
        $graph->title->SetFont(FF_DV_SANSSERIF, FS_NORMAL, 12);
        $graph->legend->Pos(0.1, 0.2);
        
        $p1 = new PiePlot3d($sectors);
        $p1->SetCenter(0.45, 0.5);
        $p1->value->SetFont(FF_DV_SANSSERIF, FS_NORMAL, 12);
        $p1->SetLegends($legends);
        
        $graph->Add($p1);
        $graph->Stroke();
    }

    /**
     * Метод, формирующий html таблицу статистики.
     */
    public function showTable() {
        $this->allStat();
        echo '<table class="statistics">
        <thead>
            <tr>
                <th>IP адрес клиента</th>
                <th>Браузер</th>
                <th>Общее кол-во посещений с этого IP</th>
                <th>Страница</th>
                <th>Кол-во посещений страницы</th>
                <th>Дата</th>
                <th>Время</th>
            </tr>
        </thead>
        <tbody>';

        // Key IP
        foreach ($this->all as $row) {
            echo '<tr>';
            echo '<td>'.$row['client_ip'].'</td>';
            echo '<td>'.$row['browser'].'</td>';
            echo '<td class="short">'.$this->total_sum_ip[$row['client_ip']].'</td>';
            echo '<td>'.$row['urn'].'</td>';
            echo '<td class="short">'.$row['count'].'</td>';
            echo '<td>'.$row['date'].'</td>';
            echo '<td>'.$row['time'].'</td>';
            echo '</tr>';
        }
        echo '</tbody></table>';
    }

    /**
     * Метод, формирующий html таблицу статистики с фильтрацией по полю.
     */
    public function showAdminTable($filter, $desc) {
        global $urls;
        if (isset($filter)) {
            $this->orderBy($filter, $desc);
        } else {
            $this->allStat();
        }

        echo '<table class="statistics">';
        echo '<thead><tr>';
        echo '<th>Фильтровать <a href="'.$urls['admin_statistic']
             .'&filter=id">по возрастанию</a>||<a href="'.$urls['admin_statistic']
             .'&filter=id&desc=ok">по убыванию</a></th>';
        echo '<th>Фильтровать <a href="'.$urls['admin_statistic']
             .'&filter=client_ip">по возрастанию</a>||<a href="'.$urls['admin_statistic']
             .'&filter=client_ip&desc=ok">по убыванию</a></th>';
        echo '<th>Фильтровать <a href="'.$urls['admin_statistic']
             .'&filter=browser">по возрастанию</a>||<a href="'.$urls['admin_statistic']
             .'&filter=browser&desc=ok">по убыванию</a></th>';
        echo '<th></th>';
        echo '<th>Фильтровать <a href="'.$urls['admin_statistic']
             .'&filter=urn">по возрастанию</a>||<a href="'.$urls['admin_statistic']
             .'&filter=urn&desc=ok">по убыванию</a></th>';
        echo '<th>Фильтровать <a href="'.$urls['admin_statistic']
             .'&filter=count">по возрастанию</a>||<a href="'.$urls['admin_statistic']
             .'&filter=count&desc=ok">по убыванию</a></th>';
        echo '<th>Фильтровать <a href="'.$urls['admin_statistic']
             .'&filter=date">по возрастанию</a>||<a href="'.$urls['admin_statistic']
             .'&filter=date&desc=ok">по убыванию</a></th>';
        echo '<th>Фильтровать <a href="'.$urls['admin_statistic']
             .'&filter=time">по возрастанию</a>||<a href="'.$urls['admin_statistic']
             .'&filter=time&desc=ok">по убыванию</a></th>';
        echo '<th></th>';
        echo '<th></th>';
        echo '</tr></thead>';

        echo '<tbody><tr>';
        echo '<th>ID</th>';
        echo '<th>IP адрес клиента</th>';
        echo '<th>Браузер</th>';
        echo '<th>Общее кол-во посещений с этого IP</th>';
        echo '<th>Страница</th>';
        echo '<th>Кол-во посещений страницы</th>';
        echo '<th>Дата</th>';
        echo '<th>Время</th>';
        echo '<th></th>';
        echo '<th></th>';
        echo '</tr>';
        

        // Key IP
        foreach ($this->all as $row) {
            echo '<tr>';
            echo '<td>'.$row['id'].'</td>';
            echo '<td>'.$row['client_ip'].'</td>';
            echo '<td>'.$row['browser'].'</td>';
            echo '<td class="short">'.$this->total_sum_ip[$row['client_ip']].'</td>';
            echo '<td>'.$row['urn'].'</td>';
            echo '<td class="short">'.$row['count'].'</td>';
            echo '<td>'.$row['date'].'</td>';
            echo '<td>'.$row['time'].'</td>';
            echo '<td><a href="'.$urls['admin_statistic_edit'].'&id='.$row['id']
                 .'">Редактировать</a></td>';
            echo '<td><a href="'.$urls['admin_statistic_delete'].'&id='.$row['id']
                 .'">Удалить</a></td>';
            echo '</tr>';
        }
        echo '</tbody></table>';
    }

    /**
     * Метод для вывода информации об объекте.
     */
    public function displayInfo() {
        echo $this->clientIP.'<br/>';
        echo $this->browserNameVersion;
        echo $this->date.' '.$this->time.'<br/>';
        echo $this->URN.'<br/>';
    }
}

?>