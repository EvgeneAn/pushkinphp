<?php
abstract class Model {
    protected $host = 'localhost';
    protected $port = '3306';
    protected $DbName = 'pushkin_site';
    protected $DbUsername = 'pushkin';
    protected $DbPassword = 'pushkin';
    protected $connection;
    protected $isReady = 0;
    protected $errorMessage = '';
    
    protected $table_name;
    protected $errors;
    protected $all = array();

    function __construct() {
        $this->connection = new mysqli($this->host, $this->DbUsername,
                                       $this->DbPassword, $this->DbName,
                                       $this->port);
        $this->isReady = $this->connection->connect_errno;
        
        if ($this->isReady) {
            $this->errorMessage = 'Не удалось подключиться к БД.';
        }
    }

    abstract function isValid($data, $validate);

    public function getIsReady() {
        return (!$this->isReady) ? true: false;
    }

    function getAll() {
        return $this->all;
    }

    function getErrors() {
        return $this->errors;
    }

    /**
     * Добавляет экземпляр модели в таблицу. В запрос
     * подставляются только те поля, которые указаны в $columns
     * При успешной работе возвращает true, иначе - false.
     */
    abstract function insert(array $columns);

    /**
     * Выполняет sql запрос на обновление данных. Принимает
     * $columns - столбцы, которые будут обновляться, и
     * $filter - ассоц. массив, на основе которого будет
     * выполняться сортировка записей для обновления. В
     * $filter ключ является названием столбца для филтрация,
     * а значение ключа является значением столбца.
     * 
     * Возвращает true при успешной работе, false - при ошибке.
     */
    function update($columns, $filter) {
        if ($this->getIsReady()) {
            $query = $this->getUpdateQuery($columns, $filter);
            $bind_str = $this->getBindStr($columns);
            
            if ($stmt = $this->connection->prepare($query[0])) {
                if ($stmt->bind_param($bind_str, ...array_values($query[1]))) {
                    if ($stmt->execute()) {
                        return true;
                    } else {
                        $this->errors['db_error'] = $stmt->error;
                    }
                }
            }
        } else {
            $message = 'Не удалось установить соединение с БД';
            $this->errors['db_error'] = $message;
        }
        return false;
    }
    
    /**
     * Выполняет удаление записи из таблицы по ее id.
     * Возвращает true при успешной работе, false - при ошибке.
     */
    function delete(){
        if ($this->getIsReady()) {
            $query = 'DELETE FROM '.$this->table_name.' WHERE id='.$this->id;
            if ($result = $this->connection->query($query)) {
                return true;
            }
        } else {
            $message = 'Не удалось установить соединение с БД';
            $this->errors['db_error'] = $message;
        }

        return false;
    }

    /**
     * Выбирает все записи из таблицы и помещает их в поле $this->all.
     * При успешной работе возвращает true, иначе - false.
     */
    function all($fields) {
        $this->all = array_splice($this->all, 0);

        if ($this->getIsReady()) {
            $query = 'SELECT ';
            foreach ($fields as $field) {
                $query .= $field.', ';
            }
            $query = substr_replace($query, '', -2);

            $query .= ' FROM '.$this->table_name;
    
            if ($result = $this->connection->query($query)) {
                if ($result->num_rows > 0) {
                    $this->all = $result->fetch_all(MYSQLI_ASSOC);
                    $this->errors['get_rows'] = true;
                    return true;
                }
            }
        } else {
            $message = 'Не удалось установить соединение с БД';
            $this->errors['db_error'] = $message;
        }

        return false;
    }

    /**
     * Возвращает запись, производя поиск по полю $field.
     * При успешной работе возвращает true, иначе - false.
     */
    abstract function get(array $filter);

    /**
     * Выбирает все записи, удовлетврояющие условию WHERE.
     * В условие подставляются все $key => $value, указанные
     * в $filter.
     * При успешной работе возвращает true, иначе - false
     */
    abstract function filter(array $filter);

    /**
     * Настраивает поля объекта данными из $data.
     */
    abstract protected function bind($data);

    /**
     * Строит строку подстоновки для ф-ии bind_param.
     */
    abstract protected function getBindStr($columns);

    /**
     * Генерирует INSERT sql-запрос в виде строки, для дальнейшего
     * использования в функции $mysqli->prepare и возвращает
     * поля, для подстановки в запрос.
     */
    protected function getInsertQuery($columns) {
        $query = 'INSERT INTO '.$this->table_name.'(';
        foreach($columns as $column) {
            if (isset($this->{$column}))
                $query .= $column.', ';
        }
        $query = substr_replace($query, ')', -2);
        
        // подставить VALUES и найти параметры для bind_param
        $query .= ' VALUES(';
        $bind_params = array();
        foreach($columns as $column) {
            if (isset($this->{$column})) {
                $bind_params[] = $this->{$column};
                $query .= '?, ';
            }
        }
        $query = substr_replace($query, ')', -2);

        return array($query, $bind_params);
    }

    /**
     * Генерирует UPDATE sql-запрос в виде строки, для дальнейшего
     * использования в функции $mysqli->prepare и возвращает
     * поля, для подстановки в запрос.
     */
    protected function getUpdateQuery($columns, $filter) {
        $query = 'UPDATE '.$this->table_name.' SET ';
        foreach($columns as $column) {
            $bind_params[] = $this->{$column};
            $query .= $column.'=?, ';
        }
        $query = substr_replace($query, '', -2);
        
        // подставить поля и их значения для сортировки.
        $query .= ' WHERE ';
        foreach ($filter as $field => $value) {
            $type = gettype($value);
            if (strcmp($type, 'integer') == 0) $query .= $field.'='.$value.' AND ';
            else $query .= $field.'=\''.$value.'\' AND ';
        }
        $query = substr_replace($query, '', -5);
        
        return array($query, $bind_params);
    }
}
?>