<?php
abstract class Controller {
    /**
     * Массив с поддерживаемыми действиями в контроллере.
     * ОБЯЗАТЕЛЕН К ЗАПЛНЕНИЮ
     */
    protected static array $allowed_action = array('view', 'list', 'edit', 'delete');
    protected static $base_view = '/views/base.php';

    /**
     * Определяет метод для исполнения по $_REQUEST['action'],
     * запускает его, если такого $_REQUEST['action'] нет,
     * то возвращает страницу с ошибкой. 
    */
    static function start() {
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/400.php';
        $title = 'Некорректный запрос';
        $code = 400;
        $res = true;
        
        if (isset($_REQUEST['action'])) {
            if (in_array($_REQUEST['action'], static::$allowed_action)) {
                $res = static::{$_REQUEST['action']}();
                if ($res) return;
            } else {
                $message = 'Запрос: '.$_SERVER['REQUEST_URI'].' <br/>Действие '
                           .$_REQUEST['action'].' не поддерживается';
                $_REQUEST['message'] = $message;
                $code = 400;
            }
        } else {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI']
                       .' <br/>Действие не указано.';
            $_REQUEST['message'] = $message;
            $code = 400;
            $res = true;
        }
        
        if (!$res) {
            $title = 'Ошибка на сервере';
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/500.php';
            $code = 500;
        }
        
        http_response_code($code);
        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
    }

    protected static function view() {}
    protected static function list() {}
    protected static function edit() {}
    protected static function delete() {}
}
?>