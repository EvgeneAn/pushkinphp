<?php
require $_SERVER['DOCUMENT_ROOT'].'/core/Controller.php';
require $_SERVER['DOCUMENT_ROOT'].'/models/Article.php';

class Main extends Controller {
    protected static array $allowed_action = array('view');
    
    protected static function view() {
        global $urls;
        $title = 'Главная';
        $view = 'views/MainView.php';

        $article = new Article();
        $filter = array('title' => 'БИОГРАФИЯ АЛЕКСАНДРА СЕРГЕЕВИЧА ПУШКИНА');
        $article->get($filter);
        $errors = $article->getErrors();
        
        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }
}

Main::start();
?>