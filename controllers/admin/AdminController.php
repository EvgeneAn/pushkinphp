<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/Controller.php';

class AdminController extends Controller {
    protected static array $allowed_action = array('view');
    protected static $base_view = '/views/admin/AdminBase.php';

    static function start() {
        global $urls;
        if (isset($_SESSION['username'])) {
            if (isset($_SESSION['role']) && $_SESSION['role'] == 1) {
                parent::start();
                return;
            }
            $message = 'Вы не можете получить доступ к данной части сайта.';
            header('Location: '.$urls['home'].'&notification='.$message);
        } else {
            $message = 'Вам необходимо войти на сайт.';
            header('Location: '.$urls['login'].'&notification='.$message);
        }
    }
}
?>