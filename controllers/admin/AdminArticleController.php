<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/controllers/admin/AdminController.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/models/Article.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/models/User.php';

class AdminArticleController extends AdminController {
    protected static array $allowed_action = array('view', 'list', 'edit', 'delete', 'add');

    protected static function view() {
        global $urls;
        $title = 'Администрирование: статья';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/admin/AdminArticleView.php';
        
        $article = new Article();
        $res = true;
        
        if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
            $filter = array('a.id' => (int)$_REQUEST['id']);
            $res = $article->get($filter);
        } else {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Не указан id статьи.';
            $_REQUEST['message'] = $message;
            return true;
        }
        $errors = $article->getErrors();
        
        if (!$errors['get_rows']) {
            $_REQUEST['message'] = $errors['db_error'];
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/404.php';
            $title = 'Ресурс не найден.';
            http_response_code(404);
            include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
            return true;
        }
        
        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }
        
        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function list() {
        $title = 'Администрирование: статьи';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/admin/AdminArticleList.php';

        $articles = new Article();
        $res = true;
        global $urls;

        if (isset($_REQUEST['username'])) {
                $filter = array('username' => $_REQUEST['username']);
                $res = $articles->filter($filter);
        } else {
            $fields = ['id', 'title', 'pub_date', 'edit_date', 'text', 'id_user'];
            $res = $articles->all($fields);
        }
        $errors = $articles->getErrors();

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function add() {
        global $urls;

        $users = new User();
        $fields = ['id', 'username'];
        $res = $users->all($fields);

        if ($res) {
            if (isset($_POST['btn_ok'])) {
                $article = new Article();
                $validate = ['title', 'text', 'id_user', 'pub_date', 'pub_time',
                             'edit_date', 'edit_time'];
                $form_valid = $article->isValid($_POST, $validate);

                if ($form_valid) {
                    $res = $article->insert(['title', 'text', 'pub_date', 'edit_date',
                                             'id_user']);
                    $errors = $article->getErrors();
    
                    if ($res) {
                        if (!$errors['duplicate']) {
                            $message = 'Статья успешно добавлена.';
                            header('Location: '.$urls['admin_article'].'&id='.$article->id
                                    .'&notification='.$message);
                            return true;
                        }
                    }
                } else $errors = $article->getErrors();
            }
        }

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        $title = 'Администрирование: добавление статьи';
        $btn_text = 'Добавить';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/admin/AdminArticleAdd.php';
        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function edit() {
        global $urls;

        $users = new User();
        $fields = ['id', 'username'];
        $res = $users->all($fields);

        if ($res) {
            $article = new Article();
            if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
                $filter = array('a.id' => (int)$_REQUEST['id']);
                $res = $article->get($filter);
                
                if (isset($_POST['btn_ok'])) {
                    $validate = ['title', 'text', 'pub_date', 'pub_time',
                                 'edit_date', 'edit_time', 'id_user'];
                    $form_valid = $article->isValid($_POST, $validate);
                
                    if ($form_valid) {
                        $columns = ['title', 'text', 'edit_date', 'pub_date', 'id_user'];
                        $filter = array('id' => $_REQUEST['id']);
                        $res = $article->update($columns, $filter);
        
                        if ($res) {
                            $message = 'Статья успешно отредактирована.';
                            header('Location: '.$urls['admin_article'].'&id='.$article->id
                                    .'&notification='.$message);
                            return true;
                        }
                        $errors = $article->getErrors();
                    } else $errors = $article->getErrors();
                } 
            } else {
                $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                           .'Не указан id статьи.';
                $_REQUEST['message'] = $message;
                $view = '/views/errors/400.php';
                http_response_code(400);
                include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
                return true;
            }
            $errors = $article->getErrors();
        }

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        $title = 'Администрирование: редактирование статьи';
        $btn_text = 'Редактировать';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/admin/AdminArticleAdd.php';
        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function delete() {
        global $urls;
        $title = 'Администрирование: удаление статьи';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/admin/ArticleDelete.php';

        $article = new Article();
        $res = true;

        if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
            $filter = array('a.id' => (int)$_REQUEST['id']);
            $res = $article->get($filter);
        } else {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Не указан id статьи.';
            $_REQUEST['message'] = $message;
            return false;
        }
        $errors = $article->getErrors();

        if (!$errors['get_rows']) {
            $_REQUEST['message'] = $errors['db_error'];
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/404.php';
            $title = 'Ресурс не найден.';
            http_response_code(404);
            include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
            return true;
        }
        
        if (isset($_POST['btn_ok'])) {
            $res = $article->delete();
            if ($res) {
                $message = 'Статья успешно удалена.';
                header('Location: '.$urls['admin_articles'].'&notification='.$message);
                return true;
            }
        }

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }
}

AdminArticleController::start();
?>