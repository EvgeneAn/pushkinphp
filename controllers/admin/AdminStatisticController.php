<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/controllers/admin/AdminController.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/models/Statistic.php';

class AdminStatisticController extends AdminController {
    protected static array $allowed_action = array('view', 'delete', 'edit');

    protected static function view() {
        global $urls;
        $title = 'Администрирование: статистика';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/admin/AdminStatisticView.php';
        $filter = null;
        $desc = false;
        $fields = ['id', 'client_ip', 'browser', 'urn', 'date', 'time', 'count'];

        if (isset($_REQUEST['filter'])) {
            if (in_array($_REQUEST['filter'], $fields))
                $filter = $_REQUEST['filter'];
            
            if (isset($_REQUEST['desc']) && strcmp($_REQUEST['desc'], 'ok') == 0) {
                $desc = true;
            }
        }
        
        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function edit() {
        global $urls;

        $res = true;
        $statistic = new Statistic();

        if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
            $filter = array('id' => (int)$_REQUEST['id']);
            $res = $statistic->get($filter);
            $errors = $statistic->getErrors();

            if ($errors['get_rows']) {
                if (isset($_POST['btn_ok'])) {
                    $columns = ['client_ip', 'browser', 'urn', 'date', 'time', 'count'];
                    $form_valid = $statistic->isValid($_POST, $columns);
                
                    if ($form_valid) {
                        $filter = array('id' => $_REQUEST['id']);
                        $res = $statistic->update($columns, $filter);
        
                        if ($res) {
                            $message = 'Статистика успешно отредактирована.';
                            header('Location: '.$urls['admin_statistic']
                                   .'&id='.$statistic->id.'&notification='.$message);
                            return true;
                        }
                        $errors = $statistic->getErrors();
                    } else $errors = $statistic->getErrors();
                }
            } else {
                $title = 'Ресурс не найден';
                $message = 'Статистика с id='.$_REQUEST['id'].' не найдена.';
                $_REQUEST['message'] = $message;
                $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/400.php';
                http_response_code(404);
                include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
                return true;
            }
        } else {
            $title = 'Некорректный запрос';
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Не указан id статьи.';
            $_REQUEST['message'] = $message;
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/400.php';
            http_response_code(400);
            include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
            return true;
        }
        $errors = $statistic->getErrors();
        

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        $title = 'Администрирование: редактирование статистики';
        $btn_text = 'Редактировать';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/admin/AdminStatisticEditView.php';
        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function delete() {
        global $urls;
        $statistic = new Statistic();

        if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
            $filter = array('id' => (int)$_REQUEST['id']);
            $res = $statistic->get($filter);
        } else {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Не указан id статистики.';
            $_REQUEST['message'] = $message;
            return true;
        }
        $errors = $statistic->getErrors();

        if (!$errors['get_rows']) {
            $_REQUEST['message'] = $errors['db_error'];
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/404.php';
            $title = 'Ресурс не найден.';
            http_response_code(404);
            include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
            return true;
        }

        $res = $statistic->delete();
        if ($res) {
            $message = 'Запись статистики успешно удалена.';
            header('Location: '.$urls['admin_statistic'].'&notification='.$message);
            return true;
        }

        $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                    .'Ошибка работы с БД: '.$errors['db_error'];
        $_REQUEST['message'] = $message;
        return false;
    }
}

AdminStatisticController::start();
?>