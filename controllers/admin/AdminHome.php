<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/controllers/admin/AdminController.php';

class AdminHome extends AdminController {
    protected static array $allowed_action = array('view');

    protected static function view() {
        global $urls;
        $title = 'Администрирование';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/admin/AdminView.php';

        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }
}

AdminHome::start();
?>