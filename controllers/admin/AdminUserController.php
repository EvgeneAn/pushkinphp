<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/controllers/admin/AdminController.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/models/User.php';

class AdminUserController extends AdminController {
    protected static array $allowed_action = array('view', 'list', 'edit', 'delete', 'add');

    protected static function view() {
        global $urls;
        $title = 'Администрирование: пользователь';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/admin/AdminUserView.php';
        
        $object = new User();
        $res = true;
        
        if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
            $filter = array('id' => (int)$_REQUEST['id']);
            $res = $object->get($filter);
        } else {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Не указан id пользователя.';
            $_REQUEST['message'] = $message;
            return true;
        }
        $errors = $object->getErrors();
        
        if (!$errors['get_rows']) {
            $_REQUEST['message'] = $errors['db_error'];
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/404.php';
            $title = 'Ресурс не найден.';
            http_response_code(404);
            include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
            return true;
        }
        
        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
            .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }
        
        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function list() {
        $title = 'Администрирование: пользователи';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/admin/AdminUserList.php';

        $object = new User();
        $res = true;
        global $urls;

        if (isset($_REQUEST['username'])) {
                $filter = array('username' => $_GET['username']);
                $res = $object->filter($filter);
        } else {
            $fields = ['id', 'username', 'email', 'date_registration', 'role'];
            $res = $object->all($fields);
        }
        $errors = $object->getErrors();

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function add() {
        global $urls;

        $res = true;
        $errors['db_error'] = '';
        if (isset($_POST['btn_ok'])) {
            $object = new User();
            $validate = ['username', 'email', 'password', 'password2', 'role'];
            $form_valid = $object->isValid($_POST, $validate);
            $errors = $object->getErrors();
            
            if ($form_valid) {
                $object->date_registration = date('Y-m-d');
                $res = $object->insert(['username', 'email', 'role',
                                        'date_registration', 'password']);
                $errors = $object->getErrors();

                if ($res) {
                    if (!$errors['duplicate']) {
                        $message = 'Пользователь успешно добавлен.';
                        header('Location: '.$urls['admin_user'].'&id='.$object->id
                                .'&notification='.$message);
                        return true;
                    }
                }
            }
        }

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        $title = 'Администрирование: добавление пользователя';
        $btn_text = 'Добавить';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/admin/AdminUserAdd.php';
        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function edit() {
        global $urls;
        $res = true;
        
        $object = new User();
        if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
            $filter = ['id' => $_REQUEST['id']];
            $res = $object->get($filter);
        } else {
            $message = 'Необходимо указать корректный id.';
            $_REQUEST['message'] = $message;
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/400.php';
            http_response_code(400);
            include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
            return true;
        }
        
        $errors = $object->getErrors();
        if ($res && !$errors['get_rows']) {
            $message = 'Пользователь с таким id не найден.';
            $_REQUEST['message'] = $message;
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/404.php';
            http_response_code(404);
            include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
            return true;
        }

        if (isset($_POST['btn_ok'])) {
            $validate = ['username', 'email', 'date_registration',
                         'password', 'password2', 'role'];
            $columns = ['username', 'email', 'date_registration',
                        'password', 'role'];
            if (isset($_POST['password'])) {
                if (strcmp($_POST['password'], '') == 0) {
                    $validate = ['username', 'email', 'date_registration', 'role'];
                    $columns = $validate;
                }
            }

            $form_valid = $object->isValid($_POST, $validate);
            $errors = $object->getErrors();
            
            if ($form_valid) {
                $filter = ['id' => $object->id];
                $res = $object->update($columns, $filter);
                $errors = $object->getErrors();

                if ($res) {
                    if (!$errors['duplicate']) {
                        $message = 'Пользователь успешно отредактирован.';
                        header('Location: '.$urls['admin_user'].'&id='.$object->id
                                .'&notification='.$message);
                        return true;
                    }
                }
            }
        }

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        $title = 'Администрирование: редактирование пользователя';
        $btn_text = 'Редактировать';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/admin/AdminUserAdd.php';
        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function delete() {
        global $urls;
        $title = 'Администрирование: удаление пользователя';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/admin/AdminUserDelete.php';

        $object = new User();
        $res = true;

        if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
            $filter = array('id' => (int)$_REQUEST['id']);
            $res = $object->get($filter);
        } else {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Не указан id пользователя.';
            $_REQUEST['message'] = $message;
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/400.php';
            $title = 'Ресурс не найден.';
            http_response_code(400);
            include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
            return true;
        }
        $errors = $object->getErrors();

        if (!$errors['get_rows']) {
            $_REQUEST['message'] = $errors['db_error'];
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/404.php';
            $title = 'Ресурс не найден.';
            http_response_code(404);
            include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
            return true;
        }
        
        if (isset($_POST['btn_ok'])) {
            $res = $object->delete();
            if ($res) {
                $message = 'Пользователь успешно удален.';
                header('Location: '.$urls['admin_users'].'&notification='.$message);
                return true;
            }
        }

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }
}

AdminUserController::start();
?>