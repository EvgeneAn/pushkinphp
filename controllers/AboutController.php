<?php
require $_SERVER['DOCUMENT_ROOT'].'/core/Controller.php';

class AboutController extends Controller {
    protected static array $allowed_action = array('view', 'image');
    
    protected static function view() {
        global $urls;
        if (!isset($_SESSION['username'])) {
            header('Location: '.$urls['login'].'&notification=Вам нужно войти на сайт.');
            return true;
        }

        // Определение размера файла/папки
        $size = false;
        if (isset($_REQUEST['btn_ok'])) {
            // не выходит ли путь из директории
            $index = strpos($_REQUEST['filename'], '..');

            if ($index === false) {
                $file_path = $_SERVER['DOCUMENT_ROOT'].'/'.$_REQUEST['filename'];
                $file_path = realpath($file_path);
                if ($file_path) {
                    $size = self::getFileSize($file_path);
                }
                if ($size !== false) {
                    header('Location: '.$urls['about'].'&filesize='.$size
                           .'&path='.$file_path);
                    return true;
                } else {
                    $message = 'Файл/папка '.$_REQUEST['filename'].' не существует.';
                    header('Location: '.$urls['about'].'&notification='.$message);
                }
            }
        }

        $title = 'О сайте';
        $view = 'views/statistic/AboutView.php';
        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function image() {
        global $urls;
        if (!isset($_SESSION['username'])) {
            header('Location: '.$urls['login'].'&notification=Вам нужно войти на сайт.');
            return true;
        }

        if (isset($_REQUEST['image'])) {
            global $statistics;
            if (method_exists($statistics, $_REQUEST['image'])) {
                $statistics->{$_REQUEST['image']}();
            }
        }
        return true;
    }

    private static function getFileSize($path)
    {
        $fileSize = 0;
        if (is_dir($path)) {
            $dir = scandir($path);
            foreach($dir as $file)
            {
                if (($file!='.') && ($file!='..'))
                    if(is_dir($path . '/' . $file))
                        $fileSize += self::getFileSize($path.'/'.$file);
                    else
                        $fileSize += filesize($path . '/' . $file);
            }
        } else {
            $fileSize += filesize($path);
        }

        return $fileSize;
    }
}

AboutController::start();
?>