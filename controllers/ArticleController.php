<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/core/Controller.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/models/Article.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/models/User.php';

class ArticleController extends Controller {
    protected static array $allowed_action = array('view', 'list', 'edit', 'delete', 'add');
    
    protected static function view() {
        global $urls;
        $title = 'Статья';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/article/ArticleView.php';

        $article = new Article();
        $res = true;

        if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
            $filter = array('a.id' => (int)$_REQUEST['id']);
            $res = $article->get($filter);
        } else {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
            .'Не указан id статьи.';
            $_REQUEST['message'] = $message;
            return false;
        }
        $errors = $article->getErrors();

        if (!$errors['get_rows']) {
            $_REQUEST['message'] = $errors['db_error'];
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/404.php';
            $title = 'Ресурс не найден.';
            http_response_code(404);
            include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
            return true;
        }

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function list() {
        $title = 'Статьи';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/article/ArticleListView.php';

        $articles = new Article();
        $res = true;
        global $urls;

        if (isset($_REQUEST['username'])) {
            if (isset($_SESSION['username'])) {
                if (strcmp($_SESSION['username'], $_REQUEST['username']) == 0) {
                    $filter = array('username' => $_REQUEST['username']);
                    $res = $articles->filter($filter);
                } else {
                    header('Location: '.$urls['articles']);
                    return true;
                }
            } else {
                $message = 'Войдите на сайт.';
                header('Location: '.$urls['login'].'&notification='.$message);
                return true;
            }
        } else {
            $fields = ['id', 'title', 'pub_date', 'edit_date', 'text', 'id_user'];
            $res = $articles->all($fields);
        }
        $errors = $articles->getErrors();

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function add() {
        global $urls;
        if (!isset($_SESSION['username'])) {
            header('Location: '.$urls['login'].'&notification=Вам нужно войти на сайт.');
            return true;
        }

        $res = true;
        if (isset($_POST['btn_ok'])) {
            $article = new Article();
            $form_valid = $article->isValid($_POST, ['title', 'text']);

            if ($form_valid) {
                $article->id_user = $_SESSION['id_user'];
                $article->pub_date = date('Y-m-d H:i:s');
                $article->edit_date = $article->pub_date;
                $res = $article->insert(['title', 'text', 'pub_date', 'edit_date',
                                         'id_user']);
                $errors = $article->getErrors();

                if ($res) {
                    if (!$errors['duplicate']) {
                        $message = 'Статья успешно добавлена.';
                        header('Location: '.$urls['article'].'&id='.$article->id
                                .'&notification='.$message);
                        return true;
                    }
                }
            } else $errors = $article->getErrors();
        }

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        $title = 'Добавление статьи';
        $btn_text = 'Добавить';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/article/ArticleAddView.php';
        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function edit() {
        global $urls;
        if (!isset($_SESSION['username'])) {
            header('Location: '.$urls['login'].'&notification=Вам нужно войти на сайт.');
            return true;
        }

        $res = true;
        $article = new Article();
        if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
            $filter = array('a.id' => (int)$_REQUEST['id']);
            $res = $article->get($filter);
            
            if ($_SESSION['id_user'] != $article->id_user) {
                $message = 'Вы не можете редактировать статьи других пользователей.';
                $_REQUEST['message'] = $message;
                $title = 'Недостаточно прав';
                $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/403.php';
                http_response_code(403);
                include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
                return true;
            } else {
                if (isset($_POST['btn_ok'])) {
                    $form_valid = $article->isValid($_POST, ['title', 'text']);
                
                    if ($form_valid) {
                        $article->edit_date = date('Y-m-d H:i:s');
                        $columns = ['title', 'text', 'edit_date'];
                        $filter = array('id' => $_REQUEST['id']);
                        $res = $article->update($columns, $filter);
        
                        if ($res) {
                            $message = 'Статья успешно отредактирована.';
                            header('Location: '.$urls['article'].'&id='.$article->id
                                    .'&notification='.$message);
                            return true;
                        }
                        $errors = $article->getErrors();
                    } else $errors = $article->getErrors();
                }
            }

        } else {
            $title = 'Некорректный запрос';
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Не указан id статьи.';
            $_REQUEST['message'] = $message;
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/400.php';
            http_response_code(400);
            include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
            return true;
        }
        $errors = $article->getErrors();
        

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        $title = 'Редактирование статьи';
        $btn_text = 'Редактировать';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/article/ArticleAddView.php';
        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected static function delete() {
        global $urls;
        if (!isset($_SESSION['username'])) {
            header('Location: '.$urls['login'].'&notification=Вам нужно войти на сайт.');
            return true;
        }

        $title = 'Удаление статьи';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/article/ArticleDeleteView.php';

        $article = new Article();
        $res = true;

        if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
            $filter = array('a.id' => (int)$_REQUEST['id']);
            $res = $article->get($filter);
        } else {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Не указан id статьи.';
            $_REQUEST['message'] = $message;
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/400.php';
            $title = 'Некорректный запрос.';
            http_response_code(404);
            include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
            return true;
        }
        $errors = $article->getErrors();

        if (!$errors['get_rows']) {
            $_REQUEST['message'] = $errors['db_error'];
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/404.php';
            $title = 'Ресурс не найден.';
            http_response_code(404);
            include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
            return true;
        }

        if ($_SESSION['id_user'] != $article->id_user) {
            $message = 'Вы не можете удалять статьи других пользователей.';
            $_REQUEST['message'] = $message;
            $title = 'Недостаточно прав';
            $view = $_SERVER['DOCUMENT_ROOT'].'/views/errors/403.php';
            http_response_code(403);
            include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
            return true;
        } else {
            if (isset($_POST['btn_ok'])) {
                $res = $article->delete();
                
                if ($res) {
                    $message = 'Статья успешно удалена.';
                    header('Location: '.$urls['articles'].'&notification='.$message);
                    return true;
                }
            }
        }

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                       .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }
}

ArticleController::start();
?>