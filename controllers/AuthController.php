<?php
require $_SERVER['DOCUMENT_ROOT'].'/core/Controller.php';
require $_SERVER['DOCUMENT_ROOT'].'/models/User.php';

class AuthController extends Controller {
    protected static array $allowed_action = array('login', 'logout', 'register');
    
    protected static function login() {
        global $urls;
        $title = 'Вход';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/auth/LoginView.php';
        $res = true;

        if (isset($_SESSION['username'])) {
            header('Location: '.$urls['home'].'&notification=Вы уже вошли на сайт.');
            return true;
        }

        if (isset($_POST['btn_ok'])) {
            $user = new User();
            
            $form_valid = $user->isValid($_POST, array('username', 'password'));
            if ($form_valid) {
                $filter = array('username' => $_POST['username']);
                $res = $user->get($filter);
                $errors = $user->getErrors();
                
                if ($res) {
                    if ($errors['get_rows']) {
                        if ($user->checkPassword($_POST['password'])) {
                            $_SESSION['username'] = $user->username;
                            $_SESSION['role'] = $user->role;
                            $_SESSION['id_user'] = $user->id;
                            header('Location: /index.php');
                            return true;
                        }
                        $errors = $user->getErrors();
                    } 
                } else $res = false;
            }
            $errors = $user->getErrors();
        }

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                    .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }

    protected function logout() {
        if (isset($_SESSION['username'])) session_destroy();
        header('Location: /index.php');
        return true;
    }

    protected function register() {
        global $urls;
        $title = 'Регистрация';
        $view = $_SERVER['DOCUMENT_ROOT'].'/views/auth/RegisterView.php';
        $res = true;

        if (isset($_SESSION['username'])) {
            header('Location: '.$urls['home'].'&notification=Вы не можете зарегистрироваться еще раз.');
            return true;
        }

        if (isset($_POST['btn_ok'])) {
            $user = new User();
            $_POST['role'] = 0;
            
            $form_valid = $user->isValid($_POST, array('username', 'email', 'password',
                                                       'password2'));
            if ($form_valid) {
                $user->date_registration = date('Y-m-d');
                $res = $user->insert(array('username', 'email', 'password', 'role',
                                           'date_registration'));
                $errors = $user->getErrors();
                
                if ($res) {
                    if (!$errors['duplicate']) header('Location: '.$urls['login'].'&register=ok');
                }
                
            } else $errors = $user->getErrors();
        }

        if (!$res) {
            $message = 'Запрос: '.$_SERVER['REQUEST_URI'].'<br/>'
                    .'Ошибка работы с БД: '.$errors['db_error'];
            $_REQUEST['message'] = $message;
            return false;
        }

        include $_SERVER['DOCUMENT_ROOT'].static::$base_view;
        return true;
    }
}

AuthController::start();
?>