<?php
require $_SERVER['DOCUMENT_ROOT'].'/Statistics.php';

session_start();

$styles = array(
    '/statics/css/style.css',
    '/statics/css/grid.css',
    '/statics/css/text.css'
);

$scripts = array(
    '/statics/js/scroll.js',
    '/statics/js/menu.js'
);

$URN = '';
$statistics = new Statistics();
$urls = array();
$controller = $_SERVER['DOCUMENT_ROOT'].'/controllers/Main.php';

require 'router.php';
require $controller;
?>